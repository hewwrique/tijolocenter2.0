﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin
{
    class SMS
    {
        // Acesse https://www.twilio.com/, para criar uma conta e receber as credenciais

        string url = "https://api.twilio.com/2010-04-01/Accounts/AC25ee6ef6600c2dbe15197e30ab019f1a/Messages.json";
        string authorization = "AC25ee6ef6600c2dbe15197e30ab019f1a:b32cbb881b57eb37898478f52ed39543";
        string fromNumber = "+17325876344";

        

        public async void Enviar(string celular, string mensagem)
        {
            // Prepara cliente para conectar na Api
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(authorization)));

            // Monta parâmetros da chamada Api
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("From", fromNumber);
            parms.Add("To", celular);
            parms.Add("Body", mensagem);

            FormUrlEncodedContent content = new FormUrlEncodedContent(parms);

            // Realiza chamada POST
            var response = await client.PostAsync(url, content);
        }
    }
}
