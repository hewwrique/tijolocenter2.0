﻿using BlocoCenterLogin.BlocoCenter.Estoque;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda.Estoque
{
    public partial class BaixaEstoque : Form
    {
        public BaixaEstoque()
        {
            InitializeComponent();
            CarregarComboProduto();
        }

        void CarregarComboProduto()
        {
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            List<ProdutoFornecedorDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoFornecedorDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoFornecedorDTO.nome);
            cboProduto.DataSource = lista;


           

        }



        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnBaixa_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoFornecedorDTO cat = cboProduto.SelectedItem as ProdutoFornecedorDTO;

                EstoqueDTO dto = new EstoqueDTO();
                dto.id_produto = cat.ID;
                dto.quantidade = Convert.ToInt32(txtQtd.Text.Trim());

                EstoqueBusiness a = new EstoqueBusiness();
                a.DarBaixa(dto);
                MessageBox.Show("Estoque atualizado com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {



                MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            EstoqueMenu a = new EstoqueMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            EstoqueView forn = cboProduto.SelectedItem as EstoqueView;
            int mostra = forn.Quantidade;
            



        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
