﻿using BlocoCenterLogin.BlocoCenter.Estoque;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Venda.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda
{
    public partial class BlococCenterEstoqueFornecedor : Form
    {
        Validadora a = new Validadora();

        public BlococCenterEstoqueFornecedor()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            EstoqueMenu tela = new EstoqueMenu();
           
            this.Hide();
            tela.ShowDialog();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueView> lista = business.Consultar(txtPRODU.Text);

            dgvEstoq.AutoGenerateColumns = false;
            dgvEstoq.DataSource = lista;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
         
        }

        private void txtPRODU_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
