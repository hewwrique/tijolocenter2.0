﻿using BlocoCenterLogin.Telas.Financeiro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda.Estoque
{
    public partial class EstoqueMenu : Form
    {
        public EstoqueMenu()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void EstoqueMenu_Load(object sender, EventArgs e)
        {

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            BlococCenterEstoqueFornecedor a = new BlococCenterEstoqueFornecedor();
            this.Hide();
            a.ShowDialog();
        }

        private void btnEstoque_Click(object sender, EventArgs e)
        {
            BaixaEstoque a = new BaixaEstoque();
            this.Hide();
            a.ShowDialog();
        }

        private void btnFluxo_Click(object sender, EventArgs e)
        {
            FluxoCaixa a = new FluxoCaixa();
            this.Hide();
            a.ShowDialog();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu a = new BlocoCenterMenu();
            this.Hide();
            a.ShowDialog();
        }
    }
}
