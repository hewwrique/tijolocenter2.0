﻿using BlocoCenterLogin.BlocoCenter;
using BlocoCenterLogin.BlocoCenter.Cliente;
using BlocoCenterLogin.BlocoCenter.Produto;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using BlocoCenterLogin.Telas.Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterVendasPedido : Form
    {
        BindingList<ProdutoDTO> Carrinho = new BindingList<ProdutoDTO>();


        public BlocoCenterVendasPedido()
        {
            InitializeComponent();
            CarregarCombos();
            CarregarDgv();
        }

        decimal produto = 0;
        decimal valordavenda = 0;

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();
            cboProduto.ValueMember = nameof(ProdutoDTO.id_produto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.nome);
            cboProduto.DataSource = lista;


            ClienteBusiness business2 = new ClienteBusiness();
            List<ClienteDTO> lista2 = business2.Listar();
            cboCliente.ValueMember = nameof(ClienteDTO.id_cliente);
            cboCliente.DisplayMember = nameof(ClienteDTO.nome);
            cboCliente.DataSource = lista2;

            FuncionarioBusiness business3 = new FuncionarioBusiness();
            List<FuncionarioDTO> lista3 = business3.Listar();
            cboFuncionario.ValueMember = nameof(FuncionarioDTO.ID);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = lista3;   
        }

        void CarregarDgv()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = Carrinho;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void BlocoCenterVendas_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaVendas newForm2 = new BlocoCenterConsultaVendas();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void usuario_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
               

                FuncionarioDTO cat = cboFuncionario.SelectedItem as FuncionarioDTO;
                ProdutoDTO forn = cboProduto.SelectedItem as ProdutoDTO;
                ClienteDTO cat2 = cboCliente.SelectedItem as ClienteDTO;



                VendaDTO dto = new VendaDTO();
                dto.data = DateTime.Now;
                dto.id_funcionario = cat.ID;
                dto.id_cliente = cat2.id_cliente;
                dto.Pagamento = cboformapag.Text;
                dto.quantidade = Convert.ToInt32(nudQuantidade.Value);
                dto.precototal = valordavenda;


                VendaBusiness business = new VendaBusiness();
                business.Salvar(dto, Carrinho.ToList());

                MessageBox.Show("Venda feita com sucesso.", "TijoloCenter", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex )
            {

                

                    MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ProdutoDTO dtt = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(nudQuantidade.Text);


            for (int i = 0; i < Convert.ToInt32(qtd); i++)
            {
                Carrinho.Add(dtt);
                valordavenda = valordavenda + dtt.preco;
                dgvCarrinho.AutoGenerateColumns = false;
                dgvCarrinho.DataSource = Carrinho;

                lbl0.Text = "R$ " + valordavenda.ToString();
            }

            //foreach (ProdutoDTO item in Carrinho)
            //{
            //   produto = produto + dtt.preco;
            //}

   

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
          
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            int quantica = Convert.ToInt32(nudQuantidade.Value);
            Carrinho = new BindingList<ProdutoDTO>();
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = Carrinho;
            valordavenda = 0;
            lbl0.Text = "R$ " + valordavenda.ToString();

            
        }
    }
}
