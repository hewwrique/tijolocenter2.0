﻿using BlocoCenterLogin.BlocoCenter;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Venda;
using BlocoCenterLogin.Telas.Venda.Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaVendas : Form
    {
        public BlocoCenterConsultaVendas()
        {
            InitializeComponent();
        }

        private void BlocoCenterConsultaVendas_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogistica newForm2 = new BlocoCenterLogistica();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            VendaBusiness business = new VendaBusiness();
            List<ProdutoConsultarView> lista = business.Consultar(txtCliente.Text.Trim());

            dataGridViewes.AutoGenerateColumns = false;
            dataGridViewes.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                ProdutoConsultarView pedidu = dataGridViewes.Rows[e.RowIndex].DataBoundItem as ProdutoConsultarView;

                DialogResult r = MessageBox.Show("Deseja cancelar a venda?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    VendaBusiness business = new VendaBusiness();
                    business.Remover(pedidu.ID);

                }
            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                ProdutoConsultarView pedidu = dataGridViewes.Rows[e.RowIndex].DataBoundItem as ProdutoConsultarView;

                DialogResult r = MessageBox.Show("Deseja cancelar a venda?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    VendaBusiness business = new VendaBusiness();
                    business.Remover(pedidu.ID);

                }
            }
        }
    }
}
