﻿using BlocoCenterLogin.BlocoCenter.Cliente;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Relatorios___telas_diferenciais
{
    public partial class MenuRelatorios : Form
    {
        public MenuRelatorios()
        {
            InitializeComponent();
            CarregarCombos();
        }

        public void CarregarCombos()
        {
            ClienteBusiness business2 = new ClienteBusiness();
            List<ClienteDTO> lista2 = business2.Listar();
            cboCliente.ValueMember = nameof(ClienteDTO.id_cliente);
            cboCliente.DisplayMember = nameof(ClienteDTO.nome);
            cboCliente.DataSource = lista2;

            FuncionarioBusiness business3 = new FuncionarioBusiness();
            List<FuncionarioDTO> lista3 = business3.Listar();
            cboFuncionario.ValueMember = nameof(FuncionarioDTO.ID);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = lista3;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://1drv.ms/f/s!AqE5U3cTgCJckAgwzNXjdyZ8vXW4");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu a = new BlocoCenterMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void rdofinaceiro_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ClienteDTO cata1 = cboCliente.SelectedItem as ClienteDTO;
            string NomeCliente = cata1.nome;
            string TelefoneCliente = cata1.telefone;

           
            if (radioSMScli.Checked == true)
            {
                SMS aa = new SMS();
                aa.Enviar(TelefoneCliente, txtObs.Text);
                MessageBox.Show("SMS enviado com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void MenuRelatorios_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FuncionarioDTO cata = cboFuncionario.SelectedItem as FuncionarioDTO;
            string NomeFuncionario = cata.Nome;
            string TelefoneFuncionario = cata.Telefone;
            string EmailFuncionario = cata.Email;

            if (rdbEmailFuncionario.Checked == true)
            {
                Email emem = new Email();
                emem.Mensagem = txtMensagemF.Text;
                emem.Para = cata.Email;
                emem.Assunto = NomeFuncionario + " Tijolos Center";
                emem.Enviar();
                MessageBox.Show("Email enviado com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (rdbSMSfuncionario.Checked == true)
            {
                SMS mandaai = new SMS();
                mandaai.Enviar(TelefoneFuncionario, txtMensagemF.Text);
            }
        }
    }

}
