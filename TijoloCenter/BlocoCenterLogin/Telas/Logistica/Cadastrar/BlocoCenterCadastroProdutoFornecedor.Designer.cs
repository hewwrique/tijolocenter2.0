﻿namespace BlocoCenterLogin.Telas.Logistica
{
    partial class BlocoCenterCadastroProdutoFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterCadastroProdutoFornecedor));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.lblproduto = new System.Windows.Forms.Label();
            this.txtproduto = new System.Windows.Forms.TextBox();
            this.lblpreco = new System.Windows.Forms.Label();
            this.txtpreco = new System.Windows.Forms.TextBox();
            this.btnsair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(43, 49);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 155;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnsalvar
            // 
            this.btnsalvar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsalvar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalvar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalvar.ForeColor = System.Drawing.Color.White;
            this.btnsalvar.Location = new System.Drawing.Point(741, 511);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(142, 45);
            this.btnsalvar.TabIndex = 5;
            this.btnsalvar.Text = "Salvar";
            this.btnsalvar.UseVisualStyleBackColor = false;
            this.btnsalvar.Click += new System.EventHandler(this.btnconsultar_Click);
            // 
            // lblproduto
            // 
            this.lblproduto.AutoSize = true;
            this.lblproduto.BackColor = System.Drawing.Color.Transparent;
            this.lblproduto.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblproduto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblproduto.Location = new System.Drawing.Point(572, 145);
            this.lblproduto.Name = "lblproduto";
            this.lblproduto.Size = new System.Drawing.Size(100, 30);
            this.lblproduto.TabIndex = 191;
            this.lblproduto.Text = "Produto:";
            // 
            // txtproduto
            // 
            this.txtproduto.Location = new System.Drawing.Point(669, 155);
            this.txtproduto.MaxLength = 30;
            this.txtproduto.Name = "txtproduto";
            this.txtproduto.Size = new System.Drawing.Size(352, 20);
            this.txtproduto.TabIndex = 1;
            this.txtproduto.TextChanged += new System.EventHandler(this.txtproduto_TextChanged);
            this.txtproduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtproduto_KeyPress);
            // 
            // lblpreco
            // 
            this.lblpreco.AutoSize = true;
            this.lblpreco.BackColor = System.Drawing.Color.Transparent;
            this.lblpreco.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpreco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblpreco.Location = new System.Drawing.Point(598, 180);
            this.lblpreco.Name = "lblpreco";
            this.lblpreco.Size = new System.Drawing.Size(74, 30);
            this.lblpreco.TabIndex = 194;
            this.lblpreco.Text = "Preço:";
            // 
            // txtpreco
            // 
            this.txtpreco.Location = new System.Drawing.Point(669, 190);
            this.txtpreco.MaxLength = 5;
            this.txtpreco.Name = "txtpreco";
            this.txtpreco.Size = new System.Drawing.Size(123, 20);
            this.txtpreco.TabIndex = 2;
            this.txtpreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpreco_KeyPress);
            // 
            // btnsair
            // 
            this.btnsair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsair.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsair.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsair.ForeColor = System.Drawing.Color.Transparent;
            this.btnsair.Location = new System.Drawing.Point(889, 511);
            this.btnsair.Name = "btnsair";
            this.btnsair.Size = new System.Drawing.Size(132, 45);
            this.btnsair.TabIndex = 4;
            this.btnsair.Text = "Voltar";
            this.btnsair.UseVisualStyleBackColor = false;
            this.btnsair.Click += new System.EventHandler(this.btnsair_Click_1);
            // 
            // BlocoCenterCadastroProdutoFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1076, 606);
            this.Controls.Add(this.btnsair);
            this.Controls.Add(this.txtpreco);
            this.Controls.Add(this.lblpreco);
            this.Controls.Add(this.txtproduto);
            this.Controls.Add(this.lblproduto);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterCadastroProdutoFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Produto Fornecedor";
            this.Load += new System.EventHandler(this.BlocoCenterCadastroProdutoFornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.Label lblproduto;
        private System.Windows.Forms.TextBox txtproduto;
        private System.Windows.Forms.Label lblpreco;
        private System.Windows.Forms.TextBox txtpreco;
        private System.Windows.Forms.Button btnsair;
    }
}