﻿using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Logistica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterCadastroFornecedor : Form
    {
        public BlocoCenterCadastroFornecedor()
        {
            InitializeComponent();
        }
        Validadora a = new Validadora();
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultarFornecedor newForm2 = new BlocoCenterConsultarFornecedor();
            
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.nome = txtnome.Text.Trim();
                dto.numerorua = txtnumerorua.Text.Trim();
                dto.representante = txtrepresentante.Text.Trim();
                dto.telefone = txtTelefone.Text.Trim();
                dto.rg = txtRG.Text.Trim();
                dto.cep = txtcep.Text.Trim();
                dto.cnpj = txtCNPJ.Text.Trim();
                dto.email = txtemail.Text.Trim();



                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Salvo com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
           catch (Exception)
            {

                MessageBox.Show("Tente novamente mais tarde", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
       }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void BlocoCenterCadastroFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            BlocoCenterAlterarFornecedor newForm2 = new BlocoCenterAlterarFornecedor();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void btncadastroproduto_Click(object sender, EventArgs e)
        {

            BlocoCenterCadastroProdutoFornecedor newForm2 = new BlocoCenterCadastroProdutoFornecedor();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void btncadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.nome = txtnome.Text.Trim();
                dto.numerorua = txtnumerorua.Text.Trim();
                dto.representante = txtrepresentante.Text.Trim();
                dto.telefone = txtTelefone.Text.Trim();
                dto.rg = txtRG.Text.Trim();
                dto.cep = txtcep.Text.Trim();
                dto.cnpj = txtCNPJ.Text.Trim();
                dto.email = txtemail.Text.Trim();



                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Salvo com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (Exception ex)
            {

                MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultarFornecedor newForm2 = new BlocoCenterConsultarFornecedor();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtcep_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCNPJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtRG_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
        
        }

        private void txtRG_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtrepresentante_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtrepresentante_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void txtrepresentante_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtnumerorua_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtcep_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click_1(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }
    }
}
