﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Logistica
{
    public partial class BlocoCenterCadastroProdutoFornecedor : Form
    {
        Validadora a = new Validadora();

        public BlocoCenterCadastroProdutoFornecedor()
        {
            InitializeComponent();
        }

        private void BlocoCenterCadastroProdutoFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoFornecedorDTO dto = new ProdutoFornecedorDTO();
                dto.nome = txtproduto.Text.Trim();
                dto.preco = Convert.ToDecimal(txtpreco.Text);


                ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

                {
                    MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

           
        }

        private void button1_Click(object sender, EventArgs e)
        {

            BlocoCenterConsultaProduto newForm2 = new BlocoCenterConsultaProduto();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            BlocoCenterAlterarProdutoF newForm2 = new BlocoCenterAlterarProdutoF();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtproduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void btnsair_Click_1(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }
    }
}
