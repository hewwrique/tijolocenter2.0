﻿namespace BlocoCenterLogin
{
    partial class BlocoCenterCadastroFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterCadastroFornecedor));
            this.lblrepresentante = new System.Windows.Forms.Label();
            this.lblrg = new System.Windows.Forms.Label();
            this.lblnome = new System.Windows.Forms.Label();
            this.lblemail = new System.Windows.Forms.Label();
            this.lblcnpj = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtrepresentante = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtcep = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.btncadastrar = new System.Windows.Forms.Button();
            this.txtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtnumerorua = new System.Windows.Forms.TextBox();
            this.lblnumerorua = new System.Windows.Forms.Label();
            this.btnsair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblrepresentante
            // 
            this.lblrepresentante.AutoSize = true;
            this.lblrepresentante.BackColor = System.Drawing.Color.Transparent;
            this.lblrepresentante.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrepresentante.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblrepresentante.Location = new System.Drawing.Point(484, 254);
            this.lblrepresentante.Name = "lblrepresentante";
            this.lblrepresentante.Size = new System.Drawing.Size(159, 30);
            this.lblrepresentante.TabIndex = 105;
            this.lblrepresentante.Text = "Representante:";
            this.lblrepresentante.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblrg
            // 
            this.lblrg.AutoSize = true;
            this.lblrg.BackColor = System.Drawing.Color.Transparent;
            this.lblrg.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblrg.Location = new System.Drawing.Point(596, 193);
            this.lblrg.Name = "lblrg";
            this.lblrg.Size = new System.Drawing.Size(48, 30);
            this.lblrg.TabIndex = 107;
            this.lblrg.Text = "RG:";
            this.lblrg.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.BackColor = System.Drawing.Color.Transparent;
            this.lblnome.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblnome.Location = new System.Drawing.Point(565, 104);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(79, 30);
            this.lblnome.TabIndex = 108;
            this.lblnome.Text = "Nome:";
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.BackColor = System.Drawing.Color.Transparent;
            this.lblemail.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblemail.Location = new System.Drawing.Point(565, 224);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(80, 30);
            this.lblemail.TabIndex = 111;
            this.lblemail.Text = "E-mail:";
            // 
            // lblcnpj
            // 
            this.lblcnpj.AutoSize = true;
            this.lblcnpj.BackColor = System.Drawing.Color.Transparent;
            this.lblcnpj.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcnpj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblcnpj.Location = new System.Drawing.Point(574, 134);
            this.lblcnpj.Name = "lblcnpj";
            this.lblcnpj.Size = new System.Drawing.Size(70, 30);
            this.lblcnpj.TabIndex = 112;
            this.lblcnpj.Text = "CNPJ:";
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbltelefone.Location = new System.Drawing.Point(542, 164);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(102, 30);
            this.lbltelefone.TabIndex = 113;
            this.lbltelefone.Text = "Telefone:";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.BackColor = System.Drawing.Color.Transparent;
            this.lblcep.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblcep.Location = new System.Drawing.Point(588, 307);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(56, 30);
            this.lblcep.TabIndex = 114;
            this.lblcep.Text = "CEP:";
            this.lblcep.Click += new System.EventHandler(this.label10_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(49, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // txtrepresentante
            // 
            this.txtrepresentante.Location = new System.Drawing.Point(650, 264);
            this.txtrepresentante.MaxLength = 30;
            this.txtrepresentante.Name = "txtrepresentante";
            this.txtrepresentante.Size = new System.Drawing.Size(373, 20);
            this.txtrepresentante.TabIndex = 6;
            this.txtrepresentante.TextChanged += new System.EventHandler(this.txtrepresentante_TextChanged_1);
            this.txtrepresentante.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrepresentante_KeyPress);
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(650, 234);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(373, 20);
            this.txtemail.TabIndex = 5;
            this.txtemail.TextChanged += new System.EventHandler(this.txtemail_TextChanged);
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(650, 316);
            this.txtcep.MaxLength = 8;
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(373, 20);
            this.txtcep.TabIndex = 4;
            this.txtcep.TextChanged += new System.EventHandler(this.txtcep_TextChanged);
            this.txtcep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcep_KeyPress);
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(650, 114);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(373, 20);
            this.txtnome.TabIndex = 1;
            this.txtnome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // btncadastrar
            // 
            this.btncadastrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btncadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncadastrar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncadastrar.ForeColor = System.Drawing.Color.White;
            this.btncadastrar.Location = new System.Drawing.Point(722, 477);
            this.btncadastrar.Name = "btncadastrar";
            this.btncadastrar.Size = new System.Drawing.Size(163, 45);
            this.btncadastrar.TabIndex = 10;
            this.btncadastrar.Text = "Cadastrar";
            this.btncadastrar.UseVisualStyleBackColor = false;
            this.btncadastrar.Click += new System.EventHandler(this.btncadastrar_Click);
            // 
            // txtCNPJ
            // 
            this.txtCNPJ.Location = new System.Drawing.Point(650, 143);
            this.txtCNPJ.Mask = "00.000.000/0000-00";
            this.txtCNPJ.Name = "txtCNPJ";
            this.txtCNPJ.Size = new System.Drawing.Size(373, 20);
            this.txtCNPJ.TabIndex = 2;
            this.txtCNPJ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCNPJ_KeyPress);
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(650, 203);
            this.txtRG.Mask = "00.000.000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(373, 20);
            this.txtRG.TabIndex = 7;
            this.txtRG.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtRG_MaskInputRejected);
            this.txtRG.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRG_KeyPress);
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(650, 174);
            this.txtTelefone.Mask = "(00)00000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(373, 20);
            this.txtTelefone.TabIndex = 3;
            this.txtTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefone_KeyPress);
            // 
            // txtnumerorua
            // 
            this.txtnumerorua.Location = new System.Drawing.Point(650, 290);
            this.txtnumerorua.MaxLength = 5;
            this.txtnumerorua.Name = "txtnumerorua";
            this.txtnumerorua.Size = new System.Drawing.Size(373, 20);
            this.txtnumerorua.TabIndex = 8;
            this.txtnumerorua.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnumerorua_KeyPress);
            // 
            // lblnumerorua
            // 
            this.lblnumerorua.AutoSize = true;
            this.lblnumerorua.BackColor = System.Drawing.Color.Transparent;
            this.lblnumerorua.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnumerorua.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblnumerorua.Location = new System.Drawing.Point(544, 280);
            this.lblnumerorua.Name = "lblnumerorua";
            this.lblnumerorua.Size = new System.Drawing.Size(100, 30);
            this.lblnumerorua.TabIndex = 133;
            this.lblnumerorua.Text = "Número:";
            // 
            // btnsair
            // 
            this.btnsair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsair.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsair.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsair.ForeColor = System.Drawing.Color.Transparent;
            this.btnsair.Location = new System.Drawing.Point(891, 477);
            this.btnsair.Name = "btnsair";
            this.btnsair.Size = new System.Drawing.Size(132, 45);
            this.btnsair.TabIndex = 10;
            this.btnsair.Text = "Voltar";
            this.btnsair.UseVisualStyleBackColor = false;
            this.btnsair.Click += new System.EventHandler(this.btnsair_Click_1);
            // 
            // BlocoCenterCadastroFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1078, 584);
            this.Controls.Add(this.btnsair);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.txtRG);
            this.Controls.Add(this.txtCNPJ);
            this.Controls.Add(this.btncadastrar);
            this.Controls.Add(this.lblnumerorua);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.txtcep);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtrepresentante);
            this.Controls.Add(this.txtnumerorua);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblcep);
            this.Controls.Add(this.lbltelefone);
            this.Controls.Add(this.lblcnpj);
            this.Controls.Add(this.lblemail);
            this.Controls.Add(this.lblnome);
            this.Controls.Add(this.lblrg);
            this.Controls.Add(this.lblrepresentante);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterCadastroFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Fornecedor";
            this.Load += new System.EventHandler(this.BlocoCenterCadastroFornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblrepresentante;
        private System.Windows.Forms.Label lblrg;
        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.Label lblcnpj;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtrepresentante;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtcep;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Button btncadastrar;
        private System.Windows.Forms.MaskedTextBox txtCNPJ;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.TextBox txtnumerorua;
        private System.Windows.Forms.Label lblnumerorua;
        private System.Windows.Forms.Button btnsair;
    }
}