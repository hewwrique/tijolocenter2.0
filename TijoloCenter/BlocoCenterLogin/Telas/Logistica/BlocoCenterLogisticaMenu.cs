﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Logistica
{
    public partial class BlocoCenterLogisticaMenu : Form
    {
        private Button button11;
        private Button button6;
        private Button button5;
        private Button button4;
        private Button btnsair;
        private PictureBox pictureBox1;

        public BlocoCenterLogisticaMenu()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterLogisticaMenu));
            this.button11 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnsair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button11.FlatAppearance.BorderSize = 4;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button11.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.Transparent;
            this.button11.Location = new System.Drawing.Point(671, 166);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(320, 45);
            this.button11.TabIndex = 1;
            this.button11.Text = "Cadastro de Fornecedor";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Transparent;
            this.button6.Location = new System.Drawing.Point(671, 229);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(320, 45);
            this.button6.TabIndex = 2;
            this.button6.Text = "Consulta de Fornecedor";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Transparent;
            this.button5.Location = new System.Drawing.Point(671, 292);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(320, 45);
            this.button5.TabIndex = 3;
            this.button5.Text = "Cadastro de Produtos";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Transparent;
            this.button4.Location = new System.Drawing.Point(671, 355);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(320, 45);
            this.button4.TabIndex = 4;
            this.button4.Text = "Consulta de Produtos";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(128, 155);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 153;
            this.pictureBox1.TabStop = false;
            // 
            // btnsair
            // 
            this.btnsair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsair.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsair.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsair.ForeColor = System.Drawing.Color.Transparent;
            this.btnsair.Location = new System.Drawing.Point(859, 451);
            this.btnsair.Name = "btnsair";
            this.btnsair.Size = new System.Drawing.Size(132, 44);
            this.btnsair.TabIndex = 5;
            this.btnsair.Text = "Voltar";
            this.btnsair.UseVisualStyleBackColor = false;
            this.btnsair.Click += new System.EventHandler(this.btnsair_Click);
            // 
            // BlocoCenterLogisticaMenu
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(246)))), ((int)(((byte)(216)))));
            this.ClientSize = new System.Drawing.Size(1077, 581);
            this.Controls.Add(this.btnsair);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterLogisticaMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Logística";
            this.Load += new System.EventHandler(this.BlocoCenterLogisticaMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroProdutoFornecedor newForm2 = new BlocoCenterCadastroProdutoFornecedor();

            this.Hide();
            newForm2.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroFornecedor newForm2 = new BlocoCenterCadastroFornecedor();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultarFornecedor newForm2 = new BlocoCenterConsultarFornecedor();

            this.Hide();
            newForm2.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaProduto a = new BlocoCenterConsultaProduto();
            this.Hide();
            a.ShowDialog();
            
          
        }

        private void BlocoCenterLogisticaMenu_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            BlocoCenterMenu AF = new BlocoCenterMenu();
            AF.ShowDialog();
            
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();

            BlocoCenterMenu AF = new BlocoCenterMenu();
            AF.ShowDialog();

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            this.Hide();

            BlocoCenterMenu AF = new BlocoCenterMenu();
            AF.ShowDialog();
        }
    }
}
