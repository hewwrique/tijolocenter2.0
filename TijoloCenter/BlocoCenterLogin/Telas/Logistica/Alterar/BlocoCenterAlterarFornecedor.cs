﻿using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Logistica
{
    public partial class BlocoCenterAlterarFornecedor : Form
    {

   
        Validadora a = new Validadora();

        public BlocoCenterAlterarFornecedor()
        {
            InitializeComponent();
        }
        FornecedorDTO Forne;

        public void LoadScreen(FornecedorDTO fornecedor)
        {
            this.Forne = fornecedor;

            lbl0.Text = fornecedor.id_fornecedor.ToString();


            txtnome.Text = fornecedor.nome;
            txtcep.Text = fornecedor.cep;
            txtCNPJ.Text = fornecedor.cnpj;
            txtnumerorua.Text = fornecedor.numerorua;
            txtrepresentante.Text = fornecedor.representante;
            txtRG.Text = fornecedor.rg;
            txtTelefone.Text = fornecedor.telefone;
            txtemail.Text = fornecedor.email;

        }
    
        private void BlocoCenterAlterarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroFornecedor newForm2 = new BlocoCenterCadastroFornecedor();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Forne.nome = txtnome.Text.Trim();
                this.Forne.cep = txtcep.Text.Trim();
                this.Forne.cnpj = txtCNPJ.Text.Trim();
                this.Forne.numerorua = txtnumerorua.Text.Trim();
                this.Forne.representante = txtrepresentante.Text.Trim();
                this.Forne.rg = txtRG.Text.Trim();
                this.Forne.telefone = txtTelefone.Text.Trim();
                this.Forne.email = txtemail.Text.Trim();

                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(Forne);
                MessageBox.Show("Cadastro alterado com sucesso");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void lblnome_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroFornecedor newForm2 = new BlocoCenterCadastroFornecedor();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click_1(object sender, EventArgs e)
        {
            BlocoCenterCadastroFornecedor newForm2 = new BlocoCenterCadastroFornecedor();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcep_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtrepresentante_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtrepresentante_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void txtnumerorua_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnumerorua_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }
    }
}
