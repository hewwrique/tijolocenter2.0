﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Logistica
{
    public partial class BlocoCenterAlterarProdutoF : Form
    {

       
        Validadora a = new Validadora();

        public BlocoCenterAlterarProdutoF()
        {
            InitializeComponent();
        }

        ProdutoFornecedorDTO  ProdutoFornecedor;

        public void LoadScreenProduto(ProdutoFornecedorDTO ProdutoF)
        {
            this.ProdutoFornecedor = ProdutoF;

            lbl0.Text = ProdutoF.ID.ToString();


          
            txtpreco.Text = ProdutoF.preco.ToString();
            txtproduto.Text = ProdutoF.nome;
        

        }
        private void btnsalvar_Click(object sender, EventArgs e)

            
        {
            try
            {
                this.ProdutoFornecedor.nome = txtproduto.Text.Trim();
                this.ProdutoFornecedor.preco = Convert.ToDecimal(txtpreco.Text.Trim());


                ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
                business.Alterar(ProdutoFornecedor);
                MessageBox.Show("Produto alterado com sucesso");
            }
            catch (Exception ex)
            {

                {
                    MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
          
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroProdutoFornecedor newForm2 = new BlocoCenterCadastroProdutoFornecedor();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroProdutoFornecedor newForm2 = new BlocoCenterCadastroProdutoFornecedor();
            this.Hide();
            newForm2.ShowDialog();

        }

        private void btnsair_Click_1(object sender, EventArgs e)
        {
            BlocoCenterCadastroProdutoFornecedor newForm2 = new BlocoCenterCadastroProdutoFornecedor();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }
    }
}
