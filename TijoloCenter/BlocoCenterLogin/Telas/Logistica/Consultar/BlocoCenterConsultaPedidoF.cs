﻿using BlocoCenterLogin.BlocoCenter.PedidoFornecedor;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Logistica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaPedidoF : Form
    {
        Validadora a = new Validadora();

        public BlocoCenterConsultaPedidoF()
        {
            InitializeComponent();
        }

        private void BlocoCenterConsultaPedido_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PedidoFornecedorBusiness business = new PedidoFornecedorBusiness();
            List<PedidoFornecedorConsultarView> lista = business.Consultar(txtpesquisa.Text.Trim());

            dgvpedido.AutoGenerateColumns = false;
            dgvpedido.DataSource = lista;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                PedidoFornecedorConsultarView pedidu = dgvpedido.Rows[e.RowIndex].DataBoundItem as PedidoFornecedorConsultarView;

                DialogResult r = MessageBox.Show("Deseja cancelar o pedido?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    PedidoFornecedorBusiness business = new PedidoFornecedorBusiness();
                    business.Remover(pedidu.ID);

                }
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click_1(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtpesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
