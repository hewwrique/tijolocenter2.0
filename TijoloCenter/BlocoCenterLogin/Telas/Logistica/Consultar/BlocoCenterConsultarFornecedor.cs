﻿using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Logistica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{

    

    public partial class BlocoCenterConsultarFornecedor : Form
    {
        Validadora a = new Validadora();

        public BlocoCenterConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Consultar(txtpesquisa.Text);



            dgvfornecedor.AutoGenerateColumns = false;
            dgvfornecedor.DataSource = lista;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 10)
            {
                FornecedorDTO fornecedor = dgvfornecedor.CurrentRow.DataBoundItem as FornecedorDTO;

                BlocoCenterAlterarFornecedor tela = new BlocoCenterAlterarFornecedor();
                tela.LoadScreen(fornecedor);
                tela.ShowDialog();


            }
            if (e.ColumnIndex == 9)
            {
                FornecedorDTO funcio = dgvfornecedor.Rows[e.RowIndex].DataBoundItem as FornecedorDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cadastro?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Remover(funcio.id_fornecedor);

                }
            }
        }

        private void BlocoCenterConsultarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void txtpesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
