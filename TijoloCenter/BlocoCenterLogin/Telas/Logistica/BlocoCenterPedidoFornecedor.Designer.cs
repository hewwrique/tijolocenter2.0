﻿namespace BlocoCenterLogin
{
    partial class BlocoCenterPedidoFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterPedidoFornecedor));
            this.lblquantidade = new System.Windows.Forms.Label();
            this.lblfornecedor = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cboFor = new System.Windows.Forms.ComboBox();
            this.nudQuantidade = new System.Windows.Forms.NumericUpDown();
            this.lblproduto = new System.Windows.Forms.Label();
            this.cboPro = new System.Windows.Forms.ComboBox();
            this.dgvCarrinho = new System.Windows.Forms.DataGridView();
            this.Produtos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblpagamento = new System.Windows.Forms.Label();
            this.cboFormadepagam = new System.Windows.Forms.ComboBox();
            this.btnpedido = new System.Windows.Forms.Button();
            this.btncarrinho = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarrinho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblquantidade
            // 
            this.lblquantidade.AutoSize = true;
            this.lblquantidade.BackColor = System.Drawing.Color.Transparent;
            this.lblquantidade.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblquantidade.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblquantidade.Location = new System.Drawing.Point(554, 221);
            this.lblquantidade.Name = "lblquantidade";
            this.lblquantidade.Size = new System.Drawing.Size(134, 30);
            this.lblquantidade.TabIndex = 136;
            this.lblquantidade.Text = "Quantidade:";
            // 
            // lblfornecedor
            // 
            this.lblfornecedor.AutoSize = true;
            this.lblfornecedor.BackColor = System.Drawing.Color.Transparent;
            this.lblfornecedor.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfornecedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblfornecedor.Location = new System.Drawing.Point(557, 191);
            this.lblfornecedor.Name = "lblfornecedor";
            this.lblfornecedor.Size = new System.Drawing.Size(131, 30);
            this.lblfornecedor.TabIndex = 133;
            this.lblfornecedor.Text = "Fornecedor:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 259);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 148;
            this.pictureBox1.TabStop = false;
            // 
            // cboFor
            // 
            this.cboFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFor.FormattingEnabled = true;
            this.cboFor.Location = new System.Drawing.Point(686, 199);
            this.cboFor.Name = "cboFor";
            this.cboFor.Size = new System.Drawing.Size(334, 21);
            this.cboFor.TabIndex = 2;
            // 
            // nudQuantidade
            // 
            this.nudQuantidade.Location = new System.Drawing.Point(686, 231);
            this.nudQuantidade.Name = "nudQuantidade";
            this.nudQuantidade.Size = new System.Drawing.Size(334, 20);
            this.nudQuantidade.TabIndex = 4;
            // 
            // lblproduto
            // 
            this.lblproduto.AutoSize = true;
            this.lblproduto.BackColor = System.Drawing.Color.Transparent;
            this.lblproduto.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblproduto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblproduto.Location = new System.Drawing.Point(588, 161);
            this.lblproduto.Name = "lblproduto";
            this.lblproduto.Size = new System.Drawing.Size(100, 30);
            this.lblproduto.TabIndex = 157;
            this.lblproduto.Text = "Produto:";
            this.lblproduto.Click += new System.EventHandler(this.label6_Click);
            // 
            // cboPro
            // 
            this.cboPro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPro.FormattingEnabled = true;
            this.cboPro.Location = new System.Drawing.Point(686, 169);
            this.cboPro.Name = "cboPro";
            this.cboPro.Size = new System.Drawing.Size(334, 21);
            this.cboPro.TabIndex = 1;
            // 
            // dgvCarrinho
            // 
            this.dgvCarrinho.AllowUserToAddRows = false;
            this.dgvCarrinho.AllowUserToDeleteRows = false;
            this.dgvCarrinho.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvCarrinho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCarrinho.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produtos});
            this.dgvCarrinho.Location = new System.Drawing.Point(67, 307);
            this.dgvCarrinho.Name = "dgvCarrinho";
            this.dgvCarrinho.ReadOnly = true;
            this.dgvCarrinho.Size = new System.Drawing.Size(332, 214);
            this.dgvCarrinho.TabIndex = 163;
            // 
            // Produtos
            // 
            this.Produtos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Produtos.DataPropertyName = "Nome";
            this.Produtos.HeaderText = "Produtos";
            this.Produtos.Name = "Produtos";
            this.Produtos.ReadOnly = true;
            // 
            // lblpagamento
            // 
            this.lblpagamento.AutoSize = true;
            this.lblpagamento.BackColor = System.Drawing.Color.Transparent;
            this.lblpagamento.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpagamento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblpagamento.Location = new System.Drawing.Point(460, 254);
            this.lblpagamento.Name = "lblpagamento";
            this.lblpagamento.Size = new System.Drawing.Size(228, 30);
            this.lblpagamento.TabIndex = 165;
            this.lblpagamento.Text = "Forma de Pagamento:";
            // 
            // cboFormadepagam
            // 
            this.cboFormadepagam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFormadepagam.FormattingEnabled = true;
            this.cboFormadepagam.Items.AddRange(new object[] {
            "Dinheiro",
            "Débito",
            "Crédito"});
            this.cboFormadepagam.Location = new System.Drawing.Point(686, 263);
            this.cboFormadepagam.Name = "cboFormadepagam";
            this.cboFormadepagam.Size = new System.Drawing.Size(334, 21);
            this.cboFormadepagam.TabIndex = 3;
            // 
            // btnpedido
            // 
            this.btnpedido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnpedido.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnpedido.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpedido.ForeColor = System.Drawing.Color.Transparent;
            this.btnpedido.Location = new System.Drawing.Point(417, 477);
            this.btnpedido.Name = "btnpedido";
            this.btnpedido.Size = new System.Drawing.Size(159, 44);
            this.btnpedido.TabIndex = 5;
            this.btnpedido.Text = "Fazer Pedido";
            this.btnpedido.UseVisualStyleBackColor = false;
            this.btnpedido.Click += new System.EventHandler(this.btnpedido_Click);
            // 
            // btncarrinho
            // 
            this.btncarrinho.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btncarrinho.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncarrinho.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncarrinho.ForeColor = System.Drawing.Color.Transparent;
            this.btncarrinho.Location = new System.Drawing.Point(582, 477);
            this.btncarrinho.Name = "btncarrinho";
            this.btncarrinho.Size = new System.Drawing.Size(216, 44);
            this.btncarrinho.TabIndex = 6;
            this.btncarrinho.Text = "Adc. ao Carrinho ";
            this.btncarrinho.UseVisualStyleBackColor = false;
            this.btncarrinho.Click += new System.EventHandler(this.btncarrinho_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(19, 307);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(42, 42);
            this.pictureBox2.TabIndex = 166;
            this.pictureBox2.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label5.Location = new System.Drawing.Point(860, 327);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 30);
            this.label5.TabIndex = 172;
            this.label5.Text = "Total: R$";
            // 
            // lbl0
            // 
            this.lbl0.AutoSize = true;
            this.lbl0.BackColor = System.Drawing.Color.Transparent;
            this.lbl0.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbl0.Location = new System.Drawing.Point(953, 327);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(67, 30);
            this.lbl0.TabIndex = 173;
            this.lbl0.Text = "00,00";
            this.lbl0.Click += new System.EventHandler(this.lbl0_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(917, 478);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 44);
            this.button1.TabIndex = 8;
            this.button1.Text = "Voltar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Transparent;
            this.button2.Location = new System.Drawing.Point(804, 477);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 45);
            this.button2.TabIndex = 7;
            this.button2.Text = "Limpar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // BlocoCenterPedidoFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1075, 584);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl0);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btncarrinho);
            this.Controls.Add(this.btnpedido);
            this.Controls.Add(this.cboFormadepagam);
            this.Controls.Add(this.lblpagamento);
            this.Controls.Add(this.dgvCarrinho);
            this.Controls.Add(this.cboPro);
            this.Controls.Add(this.lblproduto);
            this.Controls.Add(this.nudQuantidade);
            this.Controls.Add(this.cboFor);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblquantidade);
            this.Controls.Add(this.lblfornecedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterPedidoFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pedido de Produto";
            this.Load += new System.EventHandler(this.BlocoCenterPedidoFornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarrinho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblquantidade;
        private System.Windows.Forms.Label lblfornecedor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cboFor;
        private System.Windows.Forms.NumericUpDown nudQuantidade;
        private System.Windows.Forms.Label lblproduto;
        private System.Windows.Forms.ComboBox cboPro;
        private System.Windows.Forms.DataGridView dgvCarrinho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produtos;
        private System.Windows.Forms.Label lblpagamento;
        private System.Windows.Forms.ComboBox cboFormadepagam;
        private System.Windows.Forms.Button btnpedido;
        private System.Windows.Forms.Button btncarrinho;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl0;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}