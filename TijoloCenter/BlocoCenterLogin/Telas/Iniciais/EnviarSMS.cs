﻿using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Iniciais
{
    public partial class TelaSMS : Form
    {
        public TelaSMS()
        {
            InitializeComponent();
            CarregarCombinho();
        }

        void CarregarCombinho()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFun.ValueMember = nameof(FuncionarioDTO.ID);
            cboFun.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFun.DataSource = lista;
        }

        private void btnFinan_Click(object sender, EventArgs e)
        {
            FuncionarioDTO cat = cboFun.SelectedItem as FuncionarioDTO;
         
           


            string celular = cat.Telefone;
            string nome = cat.Nome;
            string senhasms = cat.Senha;
            string celularInformado = txtTelefone.Text;

            string mensagem = "Sr(a) " + nome + " foi solicitado uma recuperação de senha no sistema Tijolos Center, sua senha é: " + senhasms + " Grato, EQUIPE FENIX";

            //if (celularInformado == celular)
           // {
                SMS sms = new SMS();
                sms.Enviar(celularInformado, mensagem);
                MessageBox.Show("SMS enviado com sucesso", "Fenix", MessageBoxButtons.OK, MessageBoxIcon.Information);
          //  }
           
 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterLogin a = new BlocoCenterLogin();
            this.Hide();
            a.ShowDialog();
        }
    }
}
