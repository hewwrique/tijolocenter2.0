﻿using BlocoCenterLogin.Funcionario;
using BlocoCenterLogin.Telas.Financeiro;
using BlocoCenterLogin.Telas.Iniciais;
using BlocoCenterLogin.Telas.Logistica;
using BlocoCenterLogin.Telas.Relatorios___telas_diferenciais;
using BlocoCenterLogin.Telas.Venda;
using BlocoCenterLogin.Telas.Venda.Compra;
using BlocoCenterLogin.Telas.Venda.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterMenu : Form
    {
        public BlocoCenterMenu()
        {
            InitializeComponent();
            
        }


        void VerificarPermi()
        {

            btnCompras.Enabled = false;
            btnFinan.Enabled = false;
            btnLogistica.Enabled = false;
            btnRH.Enabled = false;
            btnVendas.Enabled = false;
            btnRelatorio.Enabled = true;

            if (UserSession.UsuarioLogado.permissao_adm == true)
            {
                btnCompras.Enabled = true;
                btnFinan.Enabled = true;
                btnLogistica.Enabled = true;
                btnRH.Enabled = true;
                btnVendas.Enabled = true;
            }

            
            if (UserSession.UsuarioLogado.permissao_adm == false)
            {
                if (UserSession.UsuarioLogado.permissao_vendas == true)
                {
                 
                    btnVendas.Enabled = true;
                 
                }

                if (UserSession.UsuarioLogado.permissao_financeiro == true)
                {
                
                    btnFinan.Enabled = true;
               
                }
                if (UserSession.UsuarioLogado.permissao_compras == true)
                {
                    btnCompras.Enabled = true;
                  
                }
                if (UserSession.UsuarioLogado.permissao_logistica == true)
                {
                   
                    btnLogistica.Enabled = true;
        

                }
                if (UserSession.UsuarioLogado.permissao_rh == true)
                {
                    btnRH.Enabled = true;

                }

             
                
                   
                

            }
        }

        private void btnFinan_Click(object sender, EventArgs e)
        {
            EstoqueMenu a = new EstoqueMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void btnVendas_Click(object sender, EventArgs e)
        {
            BlocoCenterVendasMenu a = new BlocoCenterVendasMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            ComprasMenu tela = new ComprasMenu();
            this.Hide();
            tela.ShowDialog();
           
        }

        private void btnLogistica_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu tela = new BlocoCenterLogisticaMenu();
            this.Hide();
            tela.ShowDialog();
            
        }

        private void btnRH_Click(object sender, EventArgs e)
        {
            BlocoCenterRH tela = new BlocoCenterRH();
            this.Hide();
            tela.ShowDialog();
            
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            BlocoCenterLogin tela = new BlocoCenterLogin();
            this.Hide();
            tela.ShowDialog();
          
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterLogin tela = new BlocoCenterLogin();
            this.Hide();
            tela.ShowDialog();
        }

        private void btnRelatorio_Click(object sender, EventArgs e)
        {
            MenuRelatorios a = new MenuRelatorios();
            this.Hide();
            a.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
    
           
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Splash2 dois = new Splash2();
            this.Hide();
            dois.ShowDialog();
        }
    }
}
        

             





 
       
        



