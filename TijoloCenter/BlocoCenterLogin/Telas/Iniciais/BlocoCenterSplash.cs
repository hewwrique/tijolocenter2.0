﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas
{
    public partial class BlocoCenterSplash : Form
    {
        public BlocoCenterSplash()
        {
            InitializeComponent();



            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
                    BlocoCenterLogin frm = new BlocoCenterLogin();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void BlocoCenterSplash_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
