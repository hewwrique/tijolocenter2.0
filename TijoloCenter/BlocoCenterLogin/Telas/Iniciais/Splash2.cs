﻿using BlocoCenterLogin.Funcionario;
using BlocoCenterLogin.Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Iniciais
{
    public partial class Splash2 : Form
    {
        public Splash2()
        {
            
            
                InitializeComponent();
            CarregarImagem();

                Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(5000);

                    Invoke(new Action(() =>
                    {
                        BlocoCenterMenu frm = new BlocoCenterMenu();
                        frm.Show();
                        Hide();
                    }));
                });
            

           
        }
    

        
	

	
      
	

	
        public void CarregarImagem()
        {
            imgfuncionario.Image = Imagem.ConverterParaImagem(UserSession.UsuarioLogado.Imagem);
        }
       
   
        
        
         private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Splash2_Load(object sender, EventArgs e)
        {

        }
    }
}
