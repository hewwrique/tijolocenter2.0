﻿using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Iniciais
{
    public partial class Esqueci_a_senha : Form
    {
        public Esqueci_a_senha()
        {
            InitializeComponent();
            CarregarCombinho();
        }


        Email email = new Email();

        void CarregarCombinho()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(FuncionarioDTO.ID);
            comboBox1.DisplayMember = nameof(FuncionarioDTO.Nome);
            comboBox1.DataSource = lista;
        }

        private void btnFinan_Click(object sender, EventArgs e)
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            FuncionarioDTO cata = comboBox1.SelectedItem as FuncionarioDTO;
            FuncionarioDTO form = comboBox1.SelectedItem as FuncionarioDTO;


            string nome = cata.Nome;
            string paraqm = textBox1.Text;
            string eemail = form.Email;
            string pega = cata.Senha;

            email.Para = paraqm;
            email.Assunto = "Recuperação de senha - Tijolos Center/FENIX";
            email.Mensagem = "Olá, " + nome + " foi solicitado uma recuperação de senha para sua conta no sistema Tijolos Center, sua senha" + " é " + pega + ", caso você não tenha solicitado uma recuperação, comunique o responsável de RH.                                                                         Grato EQUIPE FENIX";

            if (eemail == paraqm)
            {
                email.Enviar();
                MessageBox.Show("E-mail enviado com sucesso", "Equipe Fenix", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                MessageBox.Show("Esse e-mail não condiz com sua conta", "Equipe Fenix", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void label2_Click(object sender, EventArgs e)
        {
            TelaSMS a = new TelaSMS();
            this.Hide();
            a.ShowDialog();
        }

        private void rdoSimm_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Splash2 a = new Splash2();
            this.Hide();
            a.ShowDialog();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            BlocoCenterLogin a = new BlocoCenterLogin();
            this.Hide();
            a.ShowDialog();
        }

        private void Esqueci_a_senha_Load(object sender, EventArgs e)
        {

        }
    }
}
