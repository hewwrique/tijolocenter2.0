﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda.Compra
{
    public partial class ComprasMenu : Form
    {
        public ComprasMenu()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BlocoCenterPedidoFornecedor a = new BlocoCenterPedidoFornecedor();
            
            this.Hide();
            a.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaPedidoF a = new BlocoCenterConsultaPedidoF();
            
            this.Hide();
            a.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu ij = new BlocoCenterMenu();
            
            this.Hide();
            ij.ShowDialog();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu ij = new BlocoCenterMenu();

            this.Hide();
            ij.ShowDialog();
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu ij = new BlocoCenterMenu();

            this.Hide();
            ij.ShowDialog();
        }

        private void ComprasMenu_Load(object sender, EventArgs e)
        {

        }
    }
}
