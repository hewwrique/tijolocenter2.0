﻿using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.BlocoCenter.Produto;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda.Compra
{
    public partial class BlocoCenterAlterarProduto : Form
    {
        public BlocoCenterAlterarProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }

        
        Validadora a = new Validadora();

        ProdutoDTO pro;

        void CarregarCombo()
        {
        


            FornecedorBusiness funbus = new FornecedorBusiness();
            List<FornecedorDTO> listinha = funbus.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.nome);
            cboFornecedor.DataSource = listinha;
        }

        public void LoadScreen(ProdutoDTO produto)
        {
            this.pro = produto;

            lblIf.Text = produto.id_produto.ToString();


            txtNome.Text = produto.nome;
            txtPreco.Text = produto.preco.ToString();
            cboFornecedor.Text = produto.id_fornecedor.ToString();
            txtMensagemF.Text = produto.composição.ToString();
            
            
           
        }

        private void btncadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO cat = cboFornecedor.SelectedItem as FornecedorDTO;


                ProdutoDTO dto = new ProdutoDTO();

                this.pro.nome = txtNome.Text.Trim();
                this.pro.preco = Convert.ToDecimal(txtPreco.Text.Trim());
                this.pro.id_fornecedor = cat.id_fornecedor;
                this.pro.composição = txtMensagemF.Text.Trim() ;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Alterar(dto);

                MessageBox.Show("Produto alterado com sucesso");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            ComprasMenu  a = new ComprasMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ComprasMenu a = new ComprasMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void btnsair_Click_1(object sender, EventArgs e)
        {
            ComprasMenu a = new ComprasMenu();
            this.Hide();
            a.ShowDialog();

        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
