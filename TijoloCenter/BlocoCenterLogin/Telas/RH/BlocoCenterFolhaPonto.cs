﻿using BlocoCenterLogin.BlocoCenter.FolhadePonto;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterFolhaPonto : Form
    {
        public BlocoCenterFolhaPonto()
        {
            InitializeComponent();
            CarregarCombo();

        }


        Validadora a = new Validadora();

        public void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.ID);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {


        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label15_Click(object sender, EventArgs e)
        {
    
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterRH a = new BlocoCenterRH();
            this.Hide();
            a.ShowDialog();
        }

        private void usuario_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void maskedTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void maskedTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void maskedTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FuncionarioDTO cat = cboFuncionario.SelectedItem as FuncionarioDTO;

            FolhadePontoDTO dto = new FolhadePontoDTO();
            dto.almoco = Convert.ToDateTime(txtSaidaAlmoço.Text);
            dto.chegada = Convert.ToDateTime(txtChegada.Text);
            dto.saida = Convert.ToDateTime(txtSaida.Text);
            dto.chegadaalmoco = Convert.ToDateTime(txtChegadaAlmoço.Text);
            dto.horas = DateTime.Now.Date;
            dto.id_funcionario = cat.ID;

            FolhadePontoBusiness business = new FolhadePontoBusiness();
            business.Salvar(dto);
            MessageBox.Show("Ponto feito com sucesso", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FolhadePontoBusiness business = new FolhadePontoBusiness();
            List<FolhadePontoDTO> lista = business.Consultar(txtPesquisa.Text);



            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void dgvAAA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BlocoCenterFolhaPonto_Load(object sender, EventArgs e)
        {

        }
    }
}
