﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.Telas.RH
{
    public class CalculoFolha
    {
        public double CalcularSH(double SalarioBase)
        {

            double SH = SalarioBase / 220;
            return SH;
        }

        public double CalcularHE(double SH, double percentual, double quantidade)
        {
            double SalarioHora = SH;
            double ValorHE = SH * (percentual / 100.0);
            double HE = (ValorHE + SH) * quantidade;
            return HE;
        }

        public double CalcularDsr(double DiasT, double DominT, double HE)
        {
            double HoraExtra = HE;


            double Conta1 = HoraExtra / DiasT * DominT;
            return Conta1;
        }


        public double CalcularAtrasos(double SH, double QtdHorasAtrasadas)
        {
            double SalarioHora = SH;
            double Atraso = SalarioHora * QtdHorasAtrasadas;
            return Atraso;
        }

        public double CalcularFaltas(double SalarioBase, double QtdFaltas)
        {
            double SalarioDD = SalarioBase / 30;
            double Faltas =  SalarioDD * QtdFaltas;
            return Faltas;

        }


        double BaseINSS = 0;
        public double CalcularINSS(double SalarioBase, double HE, double DSR, double Faltas, double Atrasos)
        {

            BaseINSS = (SalarioBase + HE + DSR) - (Faltas + Atrasos);
            if (BaseINSS <= 1693.72)
            {
                double INSS1 = BaseINSS * (8 / 100.0);
                return INSS1;
            }
            else if (BaseINSS >= 1693.73 && BaseINSS <= 2822.90)
            {
                double INSS2 = BaseINSS * (9 / 100.0);
                return INSS2;
            }
            else
            {
                double INSS3 = BaseINSS * (11 / 100.0);
                return INSS3;
            }


        }




        public double CalcularFGTS(double SalarioBase)
        {
            double FGTS = SalarioBase * (8 / 100.0);
            return FGTS;
        }

        public double CalcularVT(double SalarioBase)
        {
            double VT = SalarioBase * (6 / 100.0);
            return VT;
        }

        public double CalcularIRRF(double BaseINSS, double INSS)
        {

            double BaseIRRF = BaseINSS - INSS;
            if (BaseIRRF <= 1903.98)
            {
                double IRRF1 = 0;
                IRRF1 = Math.Round(IRRF1, 2);
                return IRRF1;
            }
            else if (BaseIRRF >= 1903.99 && BaseIRRF <= 2826.65)
            {
                double IRRF2 = (BaseIRRF * (7.5 / 100.0)) - 142.80;
                IRRF2 = Math.Round(IRRF2, 2);
                return IRRF2;
            }
            else if (BaseIRRF >= 2826.66 && BaseIRRF <= 3751.05)
            {
                double IRRF3 = (BaseIRRF * (15 / 100.0)) - 354.80;
                IRRF3 = Math.Round(IRRF3, 2);
                return IRRF3;

            }
            else if (BaseIRRF >= 3751.06 && BaseIRRF <= 4664.68)
            {
                double IRRF4 = (BaseIRRF * (22.5 / 100.0)) - 636.13;
                IRRF4 = Math.Round(IRRF4, 2);
                return IRRF4;

            }
            else
            {
                double IRRF5 = (BaseIRRF * (27.5 / 100.0)) - 869.36;
                IRRF5 = Math.Round(IRRF5, 2);
                return IRRF5;

            }


        }


          

          
        

    

        public double CalcularSF(double SalarioBase, double QuantDeMenor)
        {
           

            if (SalarioBase >= 877.68 && SalarioBase <= 1319.18)
            {
                double SF = QuantDeMenor * 31.71;
                return SF;
               
            }

            else 
            {
                double SF = 0;
                return SF;

            }
         

        }
            public double CalcularSalarioLiquido(double SalarioBase, double HE, double DSR, double Desc, double Atraso, double INSS, double VT, double IRRF)
            {
                double Total = (SalarioBase + HE + DSR ) - (Desc + Atraso + INSS + VT- IRRF);
                return Total;
            }
        }

           
            }


        

    
        



    

