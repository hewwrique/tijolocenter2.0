﻿using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterCadastroUsuario : Form
    {
        public BlocoCenterCadastroUsuario()
        {
            InitializeComponent();
            CarregarCombo();
        }

        public void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cbocpf.ValueMember = nameof(FuncionarioDTO.ID);
            cbocpf.DisplayMember = nameof(FuncionarioDTO.Nome);
            cbocpf.DataSource = lista;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FuncionarioDTO cat = cbocpf.SelectedItem as FuncionarioDTO;


            

            MessageBox.Show("Permissão salva com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaFuncionario newForm2 = new BlocoCenterConsultaFuncionario();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void BlocoCenterCadastroUsuario_Load(object sender, EventArgs e)
        {

        }

        private void ckbFinan_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbAdm_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbRH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbLogistica_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbCompra_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbVenda_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaFuncionario newForm2 = new BlocoCenterConsultaFuncionario();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            FuncionarioDTO cat = cbocpf.SelectedItem as FuncionarioDTO;




           

            MessageBox.Show("Permissão salva com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }
    }
}
