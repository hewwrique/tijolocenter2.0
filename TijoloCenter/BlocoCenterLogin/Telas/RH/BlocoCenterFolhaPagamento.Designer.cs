﻿namespace BlocoCenterLogin
{
    partial class BlocoCenterFolhaPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterFolhaPagamento));
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.txtDias = new System.Windows.Forms.TextBox();
            this.txtQuant = new System.Windows.Forms.TextBox();
            this.txtAdic = new System.Windows.Forms.TextBox();
            this.nudFaltas = new System.Windows.Forms.NumericUpDown();
            this.nudAtrasos = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.DSR = new System.Windows.Forms.GroupBox();
            this.lblDSR = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDomingos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblSalarioHora = new System.Windows.Forms.Label();
            this.lblSalarioFamilia = new System.Windows.Forms.Label();
            this.lblHoraExtra = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblAtrasos = new System.Windows.Forms.Label();
            this.lblFaltas = new System.Windows.Forms.Label();
            this.lblInss = new System.Windows.Forms.Label();
            this.lblVale = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nudFilhos = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblFGTS = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblIRRF = new System.Windows.Forms.Label();
            this.btnsair = new System.Windows.Forms.Button();
            this.btnconsultar = new System.Windows.Forms.Button();
            this.btncalcular = new System.Windows.Forms.Button();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.lblSalariobase = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtrasos)).BeginInit();
            this.DSR.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFilhos)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label4.Location = new System.Drawing.Point(28, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 30);
            this.label4.TabIndex = 80;
            this.label4.Text = "Faltas:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label5.Location = new System.Drawing.Point(28, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 30);
            this.label5.TabIndex = 81;
            this.label5.Text = "Atrasos:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label6.Location = new System.Drawing.Point(679, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(168, 30);
            this.label6.TabIndex = 82;
            this.label6.Text = "Vale transporte:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label8.Location = new System.Drawing.Point(780, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 30);
            this.label8.TabIndex = 84;
            this.label8.Text = "INSS:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label13.Location = new System.Drawing.Point(711, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 30);
            this.label13.TabIndex = 88;
            this.label13.Text = "Salário base:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label2.Location = new System.Drawing.Point(670, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 30);
            this.label2.TabIndex = 89;
            this.label2.Text = "Salário por hora:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label19.Location = new System.Drawing.Point(711, 97);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(136, 30);
            this.label19.TabIndex = 95;
            this.label19.Text = "Funcionário:";
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(847, 107);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(176, 21);
            this.cboFuncionario.TabIndex = 12;
            this.cboFuncionario.SelectedIndexChanged += new System.EventHandler(this.cboFuncionario_SelectedIndexChanged);
            // 
            // txtDias
            // 
            this.txtDias.Location = new System.Drawing.Point(245, 33);
            this.txtDias.MaxLength = 2;
            this.txtDias.Name = "txtDias";
            this.txtDias.Size = new System.Drawing.Size(268, 22);
            this.txtDias.TabIndex = 1;
            this.txtDias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDias_KeyPress);
            // 
            // txtQuant
            // 
            this.txtQuant.Location = new System.Drawing.Point(203, 25);
            this.txtQuant.MaxLength = 4;
            this.txtQuant.Name = "txtQuant";
            this.txtQuant.Size = new System.Drawing.Size(59, 22);
            this.txtQuant.TabIndex = 10;
            this.txtQuant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuant_KeyPress);
            // 
            // txtAdic
            // 
            this.txtAdic.Location = new System.Drawing.Point(847, 312);
            this.txtAdic.MaxLength = 2;
            this.txtAdic.Name = "txtAdic";
            this.txtAdic.Size = new System.Drawing.Size(176, 20);
            this.txtAdic.TabIndex = 19;
            this.txtAdic.TextChanged += new System.EventHandler(this.txAdicional_TextChanged);
            this.txtAdic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAdic_KeyPress);
            // 
            // nudFaltas
            // 
            this.nudFaltas.Location = new System.Drawing.Point(104, 65);
            this.nudFaltas.Name = "nudFaltas";
            this.nudFaltas.Size = new System.Drawing.Size(37, 22);
            this.nudFaltas.TabIndex = 8;
            this.nudFaltas.ValueChanged += new System.EventHandler(this.nudFaltas_ValueChanged);
            // 
            // nudAtrasos
            // 
            this.nudAtrasos.Location = new System.Drawing.Point(122, 39);
            this.nudAtrasos.Name = "nudAtrasos";
            this.nudAtrasos.Size = new System.Drawing.Size(33, 22);
            this.nudAtrasos.TabIndex = 6;
            this.nudAtrasos.ValueChanged += new System.EventHandler(this.nudAtrasos_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label14.Location = new System.Drawing.Point(685, 332);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(162, 30);
            this.label14.TabIndex = 130;
            this.label14.Text = "Salário líquido:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblTotal.Location = new System.Drawing.Point(842, 334);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(67, 30);
            this.lblTotal.TabIndex = 20;
            this.lblTotal.Text = "00,00";
            // 
            // DSR
            // 
            this.DSR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(248)))), ((int)(((byte)(224)))));
            this.DSR.Controls.Add(this.lblDSR);
            this.DSR.Controls.Add(this.label9);
            this.DSR.Controls.Add(this.txtDomingos);
            this.DSR.Controls.Add(this.label3);
            this.DSR.Controls.Add(this.txtDias);
            this.DSR.Controls.Add(this.label1);
            this.DSR.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DSR.Location = new System.Drawing.Point(43, 47);
            this.DSR.Name = "DSR";
            this.DSR.Size = new System.Drawing.Size(586, 131);
            this.DSR.TabIndex = 135;
            this.DSR.TabStop = false;
            this.DSR.Text = "DSR";
            // 
            // lblDSR
            // 
            this.lblDSR.AutoSize = true;
            this.lblDSR.BackColor = System.Drawing.Color.Transparent;
            this.lblDSR.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDSR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblDSR.Location = new System.Drawing.Point(113, 90);
            this.lblDSR.Name = "lblDSR";
            this.lblDSR.Size = new System.Drawing.Size(67, 30);
            this.lblDSR.TabIndex = 3;
            this.lblDSR.Text = "00,00";
            this.lblDSR.Click += new System.EventHandler(this.lblDSR_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label9.Location = new System.Drawing.Point(56, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 30);
            this.label9.TabIndex = 136;
            this.label9.Text = "DSR:";
            // 
            // txtDomingos
            // 
            this.txtDomingos.Location = new System.Drawing.Point(306, 67);
            this.txtDomingos.MaxLength = 2;
            this.txtDomingos.Name = "txtDomingos";
            this.txtDomingos.Size = new System.Drawing.Size(207, 22);
            this.txtDomingos.TabIndex = 2;
            this.txtDomingos.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtDomingos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDomingos_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label3.Location = new System.Drawing.Point(56, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(244, 30);
            this.label3.TabIndex = 137;
            this.label3.Text = "Domingos Trabalhados:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label1.Location = new System.Drawing.Point(56, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 30);
            this.label1.TabIndex = 136;
            this.label1.Text = "Dias Trabalhados:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label11.Location = new System.Drawing.Point(711, 302);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 30);
            this.label11.TabIndex = 136;
            this.label11.Text = "Adicional %:";
            // 
            // lblSalarioHora
            // 
            this.lblSalarioHora.AutoSize = true;
            this.lblSalarioHora.BackColor = System.Drawing.Color.Transparent;
            this.lblSalarioHora.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalarioHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblSalarioHora.Location = new System.Drawing.Point(842, 160);
            this.lblSalarioHora.Name = "lblSalarioHora";
            this.lblSalarioHora.Size = new System.Drawing.Size(67, 30);
            this.lblSalarioHora.TabIndex = 14;
            this.lblSalarioHora.Text = "00,00";
            // 
            // lblSalarioFamilia
            // 
            this.lblSalarioFamilia.AutoSize = true;
            this.lblSalarioFamilia.BackColor = System.Drawing.Color.Transparent;
            this.lblSalarioFamilia.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalarioFamilia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblSalarioFamilia.Location = new System.Drawing.Point(191, 66);
            this.lblSalarioFamilia.Name = "lblSalarioFamilia";
            this.lblSalarioFamilia.Size = new System.Drawing.Size(67, 30);
            this.lblSalarioFamilia.TabIndex = 5;
            this.lblSalarioFamilia.Text = "00,00";
            this.lblSalarioFamilia.Click += new System.EventHandler(this.lblSalarioFamilia_Click);
            // 
            // lblHoraExtra
            // 
            this.lblHoraExtra.AutoSize = true;
            this.lblHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.lblHoraExtra.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraExtra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblHoraExtra.Location = new System.Drawing.Point(198, 48);
            this.lblHoraExtra.Name = "lblHoraExtra";
            this.lblHoraExtra.Size = new System.Drawing.Size(67, 30);
            this.lblHoraExtra.TabIndex = 11;
            this.lblHoraExtra.Text = "00,00";
            this.lblHoraExtra.Click += new System.EventHandler(this.lblHoraExtra_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label16.Location = new System.Drawing.Point(16, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(184, 30);
            this.label16.TabIndex = 142;
            this.label16.Text = "Qtd Horas Extras:";
            // 
            // lblAtrasos
            // 
            this.lblAtrasos.AutoSize = true;
            this.lblAtrasos.BackColor = System.Drawing.Color.Transparent;
            this.lblAtrasos.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtrasos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblAtrasos.Location = new System.Drawing.Point(157, 32);
            this.lblAtrasos.Name = "lblAtrasos";
            this.lblAtrasos.Size = new System.Drawing.Size(67, 30);
            this.lblAtrasos.TabIndex = 7;
            this.lblAtrasos.Text = "00,00";
            this.lblAtrasos.Click += new System.EventHandler(this.lblAtrasos_Click);
            // 
            // lblFaltas
            // 
            this.lblFaltas.AutoSize = true;
            this.lblFaltas.BackColor = System.Drawing.Color.Transparent;
            this.lblFaltas.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaltas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblFaltas.Location = new System.Drawing.Point(147, 60);
            this.lblFaltas.Name = "lblFaltas";
            this.lblFaltas.Size = new System.Drawing.Size(67, 30);
            this.lblFaltas.TabIndex = 9;
            this.lblFaltas.Text = "00,00";
            this.lblFaltas.Click += new System.EventHandler(this.lblFaltas_Click);
            // 
            // lblInss
            // 
            this.lblInss.AutoSize = true;
            this.lblInss.BackColor = System.Drawing.Color.Transparent;
            this.lblInss.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInss.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblInss.Location = new System.Drawing.Point(842, 188);
            this.lblInss.Name = "lblInss";
            this.lblInss.Size = new System.Drawing.Size(67, 30);
            this.lblInss.TabIndex = 15;
            this.lblInss.Text = "00,00";
            // 
            // lblVale
            // 
            this.lblVale.AutoSize = true;
            this.lblVale.BackColor = System.Drawing.Color.Transparent;
            this.lblVale.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblVale.Location = new System.Drawing.Point(842, 216);
            this.lblVale.Name = "lblVale";
            this.lblVale.Size = new System.Drawing.Size(67, 30);
            this.lblVale.TabIndex = 16;
            this.lblVale.Text = "00,00";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(248)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtQuant);
            this.groupBox1.Controls.Add(this.lblHoraExtra);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(43, 327);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(289, 82);
            this.groupBox1.TabIndex = 139;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Horas Extras";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(248)))), ((int)(((byte)(224)))));
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.nudAtrasos);
            this.groupBox2.Controls.Add(this.lblAtrasos);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lblFaltas);
            this.groupBox2.Controls.Add(this.nudFaltas);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(383, 196);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(246, 115);
            this.groupBox2.TabIndex = 147;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Faltas e Atrasos";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label10.Location = new System.Drawing.Point(35, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 30);
            this.label10.TabIndex = 148;
            this.label10.Text = "Dependentes:";
            // 
            // nudFilhos
            // 
            this.nudFilhos.Location = new System.Drawing.Point(185, 35);
            this.nudFilhos.Name = "nudFilhos";
            this.nudFilhos.Size = new System.Drawing.Size(37, 22);
            this.nudFilhos.TabIndex = 4;
            this.nudFilhos.ValueChanged += new System.EventHandler(this.nudFilhos_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label15.Location = new System.Drawing.Point(35, 64);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(161, 30);
            this.label15.TabIndex = 149;
            this.label15.Text = "Salario Familia:";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(248)))), ((int)(((byte)(224)))));
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.nudFilhos);
            this.groupBox3.Controls.Add(this.lblSalarioFamilia);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(43, 196);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(319, 115);
            this.groupBox3.TabIndex = 150;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Salário Família";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label7.Location = new System.Drawing.Point(778, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 30);
            this.label7.TabIndex = 151;
            this.label7.Text = "FGTS:";
            // 
            // lblFGTS
            // 
            this.lblFGTS.AutoSize = true;
            this.lblFGTS.BackColor = System.Drawing.Color.Transparent;
            this.lblFGTS.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFGTS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblFGTS.Location = new System.Drawing.Point(842, 244);
            this.lblFGTS.Name = "lblFGTS";
            this.lblFGTS.Size = new System.Drawing.Size(67, 30);
            this.lblFGTS.TabIndex = 17;
            this.lblFGTS.Text = "00,00";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label17.Location = new System.Drawing.Point(782, 272);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 30);
            this.label17.TabIndex = 153;
            this.label17.Text = "IRRF:";
            // 
            // lblIRRF
            // 
            this.lblIRRF.AutoSize = true;
            this.lblIRRF.BackColor = System.Drawing.Color.Transparent;
            this.lblIRRF.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIRRF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblIRRF.Location = new System.Drawing.Point(842, 273);
            this.lblIRRF.Name = "lblIRRF";
            this.lblIRRF.Size = new System.Drawing.Size(67, 30);
            this.lblIRRF.TabIndex = 18;
            this.lblIRRF.Text = "00,00";
            // 
            // btnsair
            // 
            this.btnsair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsair.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsair.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsair.ForeColor = System.Drawing.Color.Transparent;
            this.btnsair.Location = new System.Drawing.Point(912, 491);
            this.btnsair.Name = "btnsair";
            this.btnsair.Size = new System.Drawing.Size(111, 39);
            this.btnsair.TabIndex = 15;
            this.btnsair.Text = "Voltar";
            this.btnsair.UseVisualStyleBackColor = false;
            this.btnsair.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // btnconsultar
            // 
            this.btnconsultar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnconsultar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnconsultar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconsultar.ForeColor = System.Drawing.Color.Transparent;
            this.btnconsultar.Location = new System.Drawing.Point(646, 491);
            this.btnconsultar.Name = "btnconsultar";
            this.btnconsultar.Size = new System.Drawing.Size(143, 39);
            this.btnconsultar.TabIndex = 22;
            this.btnconsultar.Text = "Consultar";
            this.btnconsultar.UseVisualStyleBackColor = false;
            this.btnconsultar.Click += new System.EventHandler(this.btnconsultar_Click);
            // 
            // btncalcular
            // 
            this.btncalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btncalcular.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncalcular.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncalcular.ForeColor = System.Drawing.Color.Transparent;
            this.btncalcular.Location = new System.Drawing.Point(500, 491);
            this.btncalcular.Name = "btncalcular";
            this.btncalcular.Size = new System.Drawing.Size(140, 39);
            this.btncalcular.TabIndex = 21;
            this.btncalcular.Text = "Calcular";
            this.btncalcular.UseVisualStyleBackColor = false;
            this.btncalcular.Click += new System.EventHandler(this.btncalcular_Click);
            // 
            // btnsalvar
            // 
            this.btnsalvar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsalvar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsalvar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalvar.ForeColor = System.Drawing.Color.Transparent;
            this.btnsalvar.Location = new System.Drawing.Point(795, 491);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(111, 39);
            this.btnsalvar.TabIndex = 23;
            this.btnsalvar.Text = "Salvar";
            this.btnsalvar.UseVisualStyleBackColor = false;
            this.btnsalvar.Click += new System.EventHandler(this.btnsalvar_Click);
            // 
            // lblSalariobase
            // 
            this.lblSalariobase.AutoSize = true;
            this.lblSalariobase.BackColor = System.Drawing.Color.Transparent;
            this.lblSalariobase.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalariobase.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblSalariobase.Location = new System.Drawing.Point(843, 131);
            this.lblSalariobase.Name = "lblSalariobase";
            this.lblSalariobase.Size = new System.Drawing.Size(67, 30);
            this.lblSalariobase.TabIndex = 154;
            this.lblSalariobase.Text = "00,00";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::BlocoCenterLogin.Properties.Resources.olho;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(996, 134);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 23);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 156;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // BlocoCenterFolhaPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1078, 584);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblSalariobase);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.btncalcular);
            this.Controls.Add(this.btnconsultar);
            this.Controls.Add(this.DSR);
            this.Controls.Add(this.btnsair);
            this.Controls.Add(this.lblIRRF);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblFGTS);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblVale);
            this.Controls.Add(this.lblInss);
            this.Controls.Add(this.lblSalarioHora);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtAdic);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterFolhaPagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Folha de Pagamento";
            this.Load += new System.EventHandler(this.BlocoCenterFolhaPagamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAtrasos)).EndInit();
            this.DSR.ResumeLayout(false);
            this.DSR.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFilhos)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.TextBox txtDias;
        private System.Windows.Forms.TextBox txtQuant;
        private System.Windows.Forms.TextBox txtAdic;
        private System.Windows.Forms.NumericUpDown nudFaltas;
        private System.Windows.Forms.NumericUpDown nudAtrasos;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.GroupBox DSR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDomingos;
        private System.Windows.Forms.Label lblDSR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblSalarioHora;
        private System.Windows.Forms.Label lblSalarioFamilia;
        private System.Windows.Forms.Label lblHoraExtra;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblAtrasos;
        private System.Windows.Forms.Label lblFaltas;
        private System.Windows.Forms.Label lblInss;
        private System.Windows.Forms.Label lblVale;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudFilhos;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblFGTS;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblIRRF;
        private System.Windows.Forms.Button btnsair;
        private System.Windows.Forms.Button btnconsultar;
        private System.Windows.Forms.Button btncalcular;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.Label lblSalariobase;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}