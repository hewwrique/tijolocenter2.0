﻿using BlocoCenterLogin.BlocoCenter.Avaliacao;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaAvaliacao : Form
    {
        public BlocoCenterConsultaAvaliacao()
        {
            InitializeComponent();
        }
        Validadora a = new Validadora();
        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
       
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AvaliacaoBusiness business = new AvaliacaoBusiness();
            List<AvaliacaoDTO> lista = business.Consultar(txtAvaliador.Text);

           

            dgvAva.AutoGenerateColumns = false;
            dgvAva.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterAvaliacao a = new BlocoCenterAvaliacao();
            this.Hide();
            a.ShowDialog();
        }

        private void txtAvaliador_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
