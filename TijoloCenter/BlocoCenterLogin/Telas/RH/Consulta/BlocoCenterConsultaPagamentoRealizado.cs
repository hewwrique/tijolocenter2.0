﻿using BlocoCenterLogin.BlocoCenter.FolhadePagamento;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaPagamentoRealizado : Form
    {
        public BlocoCenterConsultaPagamentoRealizado()
        {
            InitializeComponent();
        }
        Validadora a = new Validadora();
        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
            List<FolhadePagamentoDTO> lista = business.Consultar(txtFolha.Text);

            dgvFolha.AutoGenerateColumns = false;
            dgvFolha.DataSource = lista;
        }

        private void dgvFolha_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 14)
            {
                FolhadePagamentoDTO paga = dgvFolha.Rows[e.RowIndex].DataBoundItem as FolhadePagamentoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o folha?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                    business.Remover(paga.id_FolhaPagamento);

                }
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterFolhaPagamento ab = new BlocoCenterFolhaPagamento();
            
            this.Hide();
            ab.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
            List<FolhadePagamentoDTO> lista = business.Consultar(txtFolha.Text);

            dgvFolha.AutoGenerateColumns = false;
            dgvFolha.DataSource = lista;
        }

        private void txtFolha_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void txtFolha_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
