﻿using BlocoCenterLogin.BlocoCenter.Cliente;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Cliente;
using BlocoCenterLogin.Telas.Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaCliente : Form
    {
        public BlocoCenterConsultaCliente()
        {
            InitializeComponent();
        }
        Validadora a = new Validadora();
        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogistica newForm2 = new BlocoCenterLogistica();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Consultar(txtpesquisa.Text);



            dgvconsultarcliente.AutoGenerateColumns = false;
            dgvconsultarcliente.DataSource = lista;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void usuario_Click(object sender, EventArgs e)
        {

        }

        private void dgvconsultarcliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                ClienteDTO funciona = dgvconsultarcliente.CurrentRow.DataBoundItem as ClienteDTO;

                BlocoCenterAlterarCliente tela = new BlocoCenterAlterarCliente();
                tela.LoadScreen(funciona);
                tela.ShowDialog();


            }
            if (e.ColumnIndex == 6)
            {
                ClienteDTO cliente = dgvconsultarcliente.Rows[e.RowIndex].DataBoundItem as ClienteDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cliente?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(cliente.id_cliente);

                }
            }
        }

        private void txtpesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
          
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }
    }
}
