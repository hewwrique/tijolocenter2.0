﻿using BlocoCenterLogin.BlocoCenter.Cliente;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Cliente
{
    public partial class BlocoCenterAlterarCliente : Form
    {
        public BlocoCenterAlterarCliente()
        {
            InitializeComponent();
        }

       
        Validadora a = new Validadora();
        ClienteDTO cli;
        public void LoadScreen(ClienteDTO cliente)
        {
            this.cli = cliente;

            label1.Text = cliente.id_cliente.ToString();


            txtnome.Text = cliente.nome;
            mtxrg.Text = cliente.rg;
            txtTelefone.Text = cliente.telefone;
            cbotipo.Text = cliente.pessoa;
            txtcpfcnpj.Text = cliente.identificacao;



        }




        private void Sair_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaCliente newForm2 = new BlocoCenterConsultaCliente();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btncadastrar_Click(object sender, EventArgs e)
        {

            try
            {
                this.cli.nome = txtnome.Text.Trim();
                this.cli.rg = mtxrg.Text.Trim();
                this.cli.telefone = txtTelefone.Text.Trim();
                this.cli.pessoa = cbotipo.Text.Trim();
                this.cli.identificacao = txtcpfcnpj.Text.Trim();

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(cli);
                MessageBox.Show("Cadastro alterado com sucesso");

            }
            catch (Exception ex)
            {

                MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            
        }

       

        private void BlocoCenterAlterarCliente_Load(object sender, EventArgs e)
        {

        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void mtxrg_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtcpfcnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaCliente newForm2 = new BlocoCenterConsultaCliente();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void mtxrg_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
           
        }

        private void txtTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void cbotipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtcpfcnpj_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcpfcnpj_KeyUp(object sender, KeyEventArgs e)
        {

        }
    }
}
    
