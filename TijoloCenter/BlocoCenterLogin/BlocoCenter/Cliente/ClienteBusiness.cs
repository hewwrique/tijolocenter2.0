﻿using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            if (dto.nome == string.Empty)
                throw new ArgumentException("Nome é obrigatório");
            if (dto.rg == string.Empty)
                throw new ArgumentException("RG é obrigatório");
            if (dto.pessoa == string.Empty)
                throw new ArgumentException("O tipo é obrigatório");
            if (dto.telefone == string.Empty)
                throw new ArgumentException("O telefone é obrigatório");
            if (dto.identificacao == string.Empty)
                throw new ArgumentException("A identificação é obrigatória");







            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }
        public List<ClienteDTO> Consultar(string cliente)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar(cliente);
        }
        public void Alterar(ClienteDTO cliente)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(cliente);
        }

        public void Remover(int id)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(id);
        }
        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }
    
    }
}
