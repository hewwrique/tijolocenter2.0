﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.Funcionario
{
    class FuncionarioBusiness
    {

        public int Salvar(FuncionarioDTO dto)
        {
            if (dto.Nome == string.Empty)          
                throw new ArgumentException("Nome é obrigatório");
            

            if (dto.CEP == string.Empty)          
               throw new ArgumentException("CEP é obrigatório");
            

            if (dto.CPF == string.Empty)           
                throw new ArgumentException("O  CPF é obrigatório");
            

            if (dto.NumeroCasa == string.Empty)       

                throw new ArgumentException("O numero da casa é obrigatório");
            

            if (dto.Email == string.Empty)       

                        throw new ArgumentException("O E-mail é obrigatório");
            
            if (dto.Cargo == string.Empty)        
                throw new ArgumentException("Cargo é obrigatório");
            
            if (dto.Salario < 954)          
                throw new ArgumentException("Esse valor deve ser maior que um salário minímo");
            
            if (dto.Sexo == string.Empty)
                throw new ArgumentException("Sexo é obrigatório");
            
            if (dto.Telefone == string.Empty)           
                throw new ArgumentException("Telefone é obrigatório");
            
            if (dto.RG == string.Empty)        
                throw new ArgumentException("o RG é obrigatório");

            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Você precisa da senha para logar no sistema");
            }



            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
             

        }
        public List<FuncionarioDTO> Consultar(string funcionario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar(funcionario);
        }
        public void Alterar(FuncionarioDTO funcionario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(funcionario);
        }

        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
             db.Remover(id);
        }
        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }

        public FuncionarioDTO Logar(string cpf, string senha)
        {
            if (cpf == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(cpf, senha);

            


        }
    }
}
