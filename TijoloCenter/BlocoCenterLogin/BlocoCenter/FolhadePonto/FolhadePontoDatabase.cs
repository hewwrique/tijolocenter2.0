﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FolhadePonto
{
    public class FolhadePontoDatabase
    {
        public int Salvar(FolhadePontoDTO dto)
        {
            string script = @" INSERT INTO tb_folhaponto (id_funcionario, dt_chegada, dt_saida, dt_horas, dt_almoco, dt_chegadaalmoco) VALUES (@id_funcionario, @dt_chegada, @dt_saida, @dt_horas, @dt_almoco, @dt_chegadaalmoco) ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_chegada", dto.chegada));
            parms.Add(new MySqlParameter("dt_chegadaalmoco", dto.chegadaalmoco));
            parms.Add(new MySqlParameter("dt_almoco", dto.almoco));
            parms.Add(new MySqlParameter("dt_saida", dto.saida));
            parms.Add(new MySqlParameter("dt_horas", dto.horas));
            parms.Add(new MySqlParameter("id_funcionario", dto.id_funcionario));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    

        public List<FolhadePontoDTO> Consultar(string dias)
        {
            string script = @"SELECT * FROM tb_folhaponto WHERE dt_horas like @dt_horas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_horas", dias + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhadePontoDTO> lista = new List<FolhadePontoDTO>();
            while (reader.Read())
            {
                FolhadePontoDTO dto = new FolhadePontoDTO();
                dto.id_folhadeponto = reader.GetInt32("id_folhaponto");
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.horas = reader.GetDateTime("dt_horas");
                dto.chegadaalmoco = reader.GetDateTime("dt_chegadaalmoco");
                dto.almoco = reader.GetDateTime("dt_almoco");
                dto.chegada = reader.GetDateTime("dt_chegada");
                
                

            

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_folhaponto WHERE id_folhaponto = @id_folhaponto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folhaponto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

       
        public List<FolhadePontoDTO> Listar()
        {
            string script = "select * from tb_folhaponto";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhadePontoDTO> listar = new List<FolhadePontoDTO>();

            while (reader.Read())
            {
                FolhadePontoDTO dto = new FolhadePontoDTO();
                dto.id_folhadeponto = reader.GetInt32("id_folhaponto");
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.chegada = reader.GetDateTime("dt_chegada");
                dto.almoco = reader.GetDateTime("dt_almoco");
                dto.chegadaalmoco = reader.GetDateTime("dt_chegadaalmoco");
                dto.saida = reader.GetDateTime("dt_saida");
                dto.horas = reader.GetDateTime("dt_horas");

                listar.Add(dto);

            }

            reader.Close();
            return listar;
        }
    }
}

