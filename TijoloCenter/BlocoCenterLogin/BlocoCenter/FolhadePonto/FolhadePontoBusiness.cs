﻿using BlocoCenterLogin.BlocoCenter.Avaliacao;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FolhadePonto
{
     class FolhadePontoBusiness
    {
        public int Salvar(FolhadePontoDTO dto)
        {
            FolhadePontoDatabase db = new FolhadePontoDatabase();
            return db.Salvar(dto);
        }

        public List<FolhadePontoDTO> Listar()
        {
            FolhadePontoDatabase db = new FolhadePontoDatabase();
            return db.Listar();
        }



        public void Remover(int id)
        {
            FolhadePontoDatabase db = new FolhadePontoDatabase();
            db.Remover(id);
        }

        public List<FolhadePontoDTO> Consultar(string dias)
        {
            FolhadePontoDatabase db = new FolhadePontoDatabase();
            return db.Consultar(dias);
        }
    }
}
