﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FolhadePonto
{
    public class FolhadePontoView
    {
        public int id_folhadeponto { get; set; }
        public DateTime chegada { get; set; }
        public DateTime almoco { get; set; }
        public DateTime chegadaalmoco { get; set; }
        public DateTime saida { get; set; }
        public DateTime horas { get; set; }
        public string id_funcionario { get; set; }
    }
}
