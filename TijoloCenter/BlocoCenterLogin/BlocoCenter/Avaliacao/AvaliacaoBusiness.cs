﻿using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Avaliacao
{
    class AvaliacaoBusiness
   {
      public int Salvar(AvaliacaoDTO dto)
       {


            

            if (dto.avaliador == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (dto.observacao == string.Empty)
            {
                throw new ArgumentException("Observação é obrigatório");
            }
            if (dto.Desempenho == string.Empty)
            {
               throw new ArgumentException("Desempenho é obrigatório");
            }


            AvaliacaoDatabase db = new AvaliacaoDatabase();
            return db.Salvar(dto);

      }

        public List<AvaliacaoDTO> Listar()
        {
            AvaliacaoDatabase db = new AvaliacaoDatabase();
            return db.Listar();
        }

       

        public void Remover(int id)
        {
            AvaliacaoDatabase db = new AvaliacaoDatabase();
            db.Remover(id);
        }

        public List<AvaliacaoDTO> Consultar(string avaliacao)
        {
            AvaliacaoDatabase db = new AvaliacaoDatabase();
            return db.Consultar(avaliacao);
        }

    }
}
