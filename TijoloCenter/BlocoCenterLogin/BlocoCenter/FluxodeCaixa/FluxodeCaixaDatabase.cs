﻿using BlocoCenterLogin.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FluxodeCaixa
{
    class FluxodeCaixaDatabase
    {
        public List<FluxoCaixaView> Consultar(string datainicio, string datafim)
        {

            string script = @"SELECT * FROM vw_consultar_fluxodecaixa WHERE dt_referencia >= @dt_inicio 
        AND dt_referencia <= @dt_fim ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_inicio", datainicio));
            parms.Add(new MySqlParameter("dt_fim", datafim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FluxoCaixaView> lista = new List<FluxoCaixaView>();
            while (reader.Read())
            {
                FluxoCaixaView dto = new FluxoCaixaView();
                dto.Referencia = reader.GetDateTime("dt_referencia");
                dto.Ganhos = reader.GetDecimal("vl_total_ganhos");
                dto.Despesas = reader.GetDouble("vl_total_despesas");




                lista.Add(dto);

            }

            reader.Close();

            return lista;
        }
    }
}
