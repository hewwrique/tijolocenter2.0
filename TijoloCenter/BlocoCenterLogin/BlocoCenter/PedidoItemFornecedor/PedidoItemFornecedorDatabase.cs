﻿using BlocoCenterLogin.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoItemFornecedor
{
    class PedidoItemFornecedorDatabase
    {
        public int Salvar(PedidoItemFornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_pedidoitemfornecedor (id_produtofornecedor, id_pedidofornecedor) VALUES (@id_produtofornecedor, @id_pedidofornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.ID_produtofornecedor));
            parms.Add(new MySqlParameter("id_pedidofornecedor", dto.ID_pedidofornecedor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoItemFornecedorDTO> ConsultarPorPedido(int idPedido)
        {
            string script = @"SELECT * FROM tb_pedidoitemfornecedor WHERE id_pedidofornecedor = @id_pedidofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedidofornecedor", idPedido));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoItemFornecedorDTO> lista = new List<PedidoItemFornecedorDTO>();
            while (reader.Read())
            {
                PedidoItemFornecedorDTO dto = new PedidoItemFornecedorDTO();
                dto.ID_pedidoitemF = reader.GetInt32("id_pedidoitemfornecedor");
                dto.ID_pedidofornecedor = reader.GetInt32("id_pedidofornecedor");
                dto.ID_produtofornecedor = reader.GetInt32("id_produtofornecedor");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

