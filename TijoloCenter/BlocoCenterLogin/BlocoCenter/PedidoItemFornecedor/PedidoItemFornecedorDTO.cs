﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoItemFornecedor
{
    class PedidoItemFornecedorDTO
    {
        public int ID_pedidoitemF { get; set; }

        public int ID_pedidofornecedor { get; set; }

        public int ID_produtofornecedor { get; set; }

    }
}
