﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoItemFornecedor
{
    class PedidoItemFornecedorBusiness
    {

        public int Salvar(PedidoItemFornecedorDTO dto)
        {
            PedidoItemFornecedorDatabase db = new PedidoItemFornecedorDatabase();
            return db.Salvar(dto); ;
        }

        public List<PedidoItemFornecedorDTO> ConsultarPorPedido(int idPedido)
        {
            PedidoItemFornecedorDatabase db = new PedidoItemFornecedorDatabase();
            return db.ConsultarPorPedido(idPedido);
        }
    }
}
