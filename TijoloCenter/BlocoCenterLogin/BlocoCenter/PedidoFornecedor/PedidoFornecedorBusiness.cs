﻿using BlocoCenterLogin.BlocoCenter.Estoque;
using BlocoCenterLogin.BlocoCenter.PedidoItemFornecedor;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoFornecedor
{
    class PedidoFornecedorBusiness
    {
        public int Salvar(PedidoFornecedorDTO pedido, List<ProdutoFornecedorDTO> produtos )
        {
            
            PedidoFornecedorDatabase pedidoDatabase = new PedidoFornecedorDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

          

            if (pedido.quantidade <= 0)
            {
                throw new ArgumentException("Quantidade é obrigatório");
            }

            EstoqueBusiness estoqueBusiness = new EstoqueBusiness();
            PedidoItemFornecedorBusiness itemBusiness = new PedidoItemFornecedorBusiness();
            foreach (ProdutoFornecedorDTO item in produtos)
            {
                PedidoItemFornecedorDTO itemDto = new PedidoItemFornecedorDTO();
                itemDto.ID_pedidofornecedor = idPedido;
                itemDto.ID_produtofornecedor = item.ID;
                itemBusiness.Salvar(itemDto);

                EstoqueDTO dt2 = new EstoqueDTO();
                dt2.id_produto = item.ID;
                dt2.quantidade = 1;
                estoqueBusiness.Adicionar(dt2);
            }

            return idPedido;

        }

        public List<PedidoFornecedorConsultarView> Consultar(string fornecedor)
        {
            PedidoFornecedorDatabase pedidoDatabase = new PedidoFornecedorDatabase();
            return pedidoDatabase.Consultar(fornecedor);
        }





        public void Alterar(PedidoFornecedorDTO dto)
        {
            PedidoFornecedorDatabase db = new PedidoFornecedorDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            PedidoFornecedorDatabase db = new PedidoFornecedorDatabase();
            db.Remover(id);
        }

        


        
    }
}
