﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoFornecedor
{
    class PedidoFornecedorDatabase
    {
        public int Salvar(PedidoFornecedorDTO dto)
        {
            string script = @" INSERT INTO tb_pedidofornecedor (ds_quantidade, vl_precototal, id_fornecedor, ds_frpagamento, dt_pedidof) VALUES (@ds_quantidade, @vl_precototal, @id_fornecedor, @ds_frpagamento, @dt_pedidof)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.id_fornecedor));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("vl_precototal", dto.precototal));
            parms.Add(new MySqlParameter("ds_frpagamento", dto.Pagamento));
            parms.Add(new MySqlParameter("dt_pedidof", dto.DataPagamento));
       


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedidofornecedor WHERE id_pedidofornecedor = @id_pedidofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedidofornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(PedidoFornecedorDTO dto)
        {
            string script = @"UPDATE tb_pedidofornecedor 
                                 SET ds_quantidade = @ds_quantidade,
                                     dt_pedidof = @dt_pedidof,
                                     ds_frpagamento = @ds_frpagamento,
                                     id_fornecedor = @id_fornecedor,
                                     vl_precototal = @vl_precototal                                  
                                     WHERE id_pedidofornecedor = @id_pedidofornecedor";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("vl_precototal", dto.precototal));
            

            Database db = new Database();
            db.ExecuteSelectScript(script, parms);

        }

          public List<PedidoFornecedorConsultarView> Consultar(string fornecedor)
        { 
        
            string script = @"SELECT * FROM vw_pedido_consultinhaa WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", fornecedor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoFornecedorConsultarView> lista = new List<PedidoFornecedorConsultarView>();
            while (reader.Read())
            {
                PedidoFornecedorConsultarView dto = new PedidoFornecedorConsultarView();
                dto.ID = reader.GetInt32("id_pedidofornecedor");
                dto.Pagamento = reader.GetString("ds_frpagamento");
                dto.QtdItens = reader.GetInt32("qtd_itens");
                dto.Fornecedor = reader.GetString("nm_fornecedor");
               dto.Data = reader.GetDateTime("dt_pedidof");
               dto.Total = reader.GetDecimal("vl_total");
               




                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


    }
}
