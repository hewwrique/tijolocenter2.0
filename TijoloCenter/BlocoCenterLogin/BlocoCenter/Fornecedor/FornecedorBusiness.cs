﻿using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Fornecedor
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO dto)
        {
            if (dto.email == string.Empty)
            {
                throw new ArgumentException("Email é obrigatório");
            }

            if (dto.cep == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório");
            }
            if (dto.cnpj == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório");
            }
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }
            if (dto.numerorua == string.Empty)
            {
                throw new ArgumentException("Número da empresa é obrigatóro ");
            }
            if (dto.representante == string.Empty)
            {
                throw new ArgumentException("Nome do representante é obrigatório");
            }
            if (dto.rg == string.Empty)
            {
                throw new ArgumentException("Número de RG");
            }
            if (dto.telefone == string.Empty)
            {
                throw new ArgumentException("Número para contato é obrigatório");
            }

            FornecedorDatabase db = new FornecedorDatabase();
            return db.Salvar(dto);
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();
        }
        public void Alterar(FornecedorDTO fornecedorr)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Alterar(fornecedorr);
        }

        

        public void Remover(int id)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Remover(id);
        }

        public List<FornecedorDTO> Consultar(string avaliacao)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Consultar(avaliacao);
        }
    }
    }

