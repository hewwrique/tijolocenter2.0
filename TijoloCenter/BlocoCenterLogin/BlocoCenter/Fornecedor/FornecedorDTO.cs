﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
   public class FornecedorDTO
    {
        public int id_fornecedor { get; set; }

        public string nome { get; set; }

        public string cnpj { get; set; }

        public string telefone { get; set; }

        public string cep { get; set; }

        public string email { get; set; }

        public string representante { get; set; }

        public string rg { get; set; }

        public string numerorua { get; set; }

    

      
    }
}
