﻿using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoItemVendas
{
    class PedidoItemVendasBusiness
    {
        public int Salvar(PedidoItemVendasDTO dto)
        {
            PedidoItemVendasDatabase db = new PedidoItemVendasDatabase();
            return db.Salvar(dto);
        }

        public List<PedidoItemVendasDTO> ConsultarPorPedido(int idPedido)
        {
            PedidoItemVendasDatabase db = new PedidoItemVendasDatabase();
            return db.ConsultarPorPedido(idPedido);
        }
    }
}
