﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
    public class PedidoItemVendasDTO
    {

        public int id_pedidoitemvendas { get; set; }

        public int id_venda { get; set; }
        public int id_produto { get; set; }
    }
}
