﻿using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (dto.preco <= 0)
            {
                throw new ArgumentException("Preço é obrigatório");
            }

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);

        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }
        public void Alterar(ProdutoDTO produto)
        {
           ProdutoDatabase db = new ProdutoDatabase();
            db.Alterar(produto);
        }

        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }
        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }


    }
}
