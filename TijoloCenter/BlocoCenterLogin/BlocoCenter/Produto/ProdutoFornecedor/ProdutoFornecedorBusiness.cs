﻿using BlocoCenterLogin.BlocoCenter.Estoque;
using BlocoCenterLogin.BlocoCenter.PedidoFornecedor;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor
{
    class ProdutoFornecedorBusiness
    {
        public int Salvar(ProdutoFornecedorDTO dto)
        {
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (dto.preco <= 0)
            {
                throw new ArgumentException("Preço é obrigatório");
            }

            ProdutoFornecedorDatabase db = new ProdutoFornecedorDatabase();
            int produtoId = db.Salvar(dto);


            EstoqueDTO estoque = new EstoqueDTO();
            estoque.id_produto = produtoId;
            estoque.quantidade = 0;

            EstoqueBusiness estoqueBusiness = new EstoqueBusiness();
            estoqueBusiness.Salvar(estoque);

            return produtoId;
        }

        public List<ProdutoFornecedorDTO> Consultar(string produtofor)
        {
            ProdutoFornecedorDatabase db = new ProdutoFornecedorDatabase();
            return db.Consultar(produtofor);
        }

        public List<ProdutoFornecedorDTO> Listar()
        {
            ProdutoFornecedorDatabase db = new ProdutoFornecedorDatabase();
            return db.Listar();
        }

        public void Alterar(ProdutoFornecedorDTO dto)
        {
            ProdutoFornecedorDatabase db = new ProdutoFornecedorDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ProdutoFornecedorDatabase  db = new ProdutoFornecedorDatabase();
            db.Remover(id);
        }
    }
}
