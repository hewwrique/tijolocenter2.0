﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @" INSERT INTO tb_produto (nm_produto, vl_preco, ds_composicao, id_fornecedor) VALUES (@nm_produto, @vl_preco, @ds_composicao, @id_fornecedor)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("vl_preco", dto.preco));
            parms.Add(new MySqlParameter("ds_composicao", dto.composição));
            parms.Add(new MySqlParameter("id_fornecedor", dto.id_fornecedor));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                               SET nm_produto = @nm_produto,
                                     vl_preco = @vl_preco
                                id_fornecedor = @id_fornecedor
                                ds_composicao = @ds_composicao
                             WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.id_produto));
            parms.Add(new MySqlParameter("ds_composicao", dto.composição));
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("vl_preco", dto.preco));
            parms.Add(new MySqlParameter("id_fornecedor", dto.id_fornecedor));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.id_produto = reader.GetInt32("id_produto");
                dto.nome = reader.GetString("nm_produto");
                dto.preco = reader.GetDecimal("vl_preco");
                dto.composição = reader.GetString("ds_composicao");
                dto.id_fornecedor = reader.GetInt32("id_fornecedor");
             


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<ProdutoDTO> Listar()
        {
            string script = "select * from tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> listar = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.id_produto = reader.GetInt32("id_produto");
                dto.nome = reader.GetString("nm_produto");
                dto.preco = reader.GetDecimal("vl_preco");
                dto.composição = reader.GetString("ds_composicao");
                dto.id_fornecedor = reader.GetInt32("id_fornecedor");

                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }
    }
}
