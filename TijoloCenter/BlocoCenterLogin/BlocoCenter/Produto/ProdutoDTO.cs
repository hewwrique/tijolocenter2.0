﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
    public class ProdutoDTO
    {
        public int id_produto { get; set; }

        public string nome { get; set; }

        public decimal preco { get; set; }       

        public string composição { get; set; }

        public int id_fornecedor { get; set; }

        public int  Quantidade { get; set; }
    }
}
