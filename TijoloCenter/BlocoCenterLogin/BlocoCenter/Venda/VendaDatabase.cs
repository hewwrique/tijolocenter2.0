﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Venda
{
    class VendaDatabase
    {
        public int Salvar(VendaDTO dto)
        {
            string script = @" INSERT INTO tb_venda (ds_quantidade, dt_data, id_funcionario, id_cliente, ds_frpagamento, vl_precototal) VALUES (@ds_quantidade, @dt_data, @id_funcionario, @id_cliente, @ds_frpagamento, @vl_precototal)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.id_cliente));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("dt_data", dto.data));
            parms.Add(new MySqlParameter("ds_frpagamento", dto.Pagamento));
            parms.Add(new MySqlParameter("id_funcionario", dto.id_funcionario));
            parms.Add(new MySqlParameter("vl_precototal", dto.precototal));






            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_venda WHERE id_venda = @id_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }



        public List<ProdutoConsultarView> Consultar(string cliente)
        {

            string script = @"SELECT * FROM vw_pedido_vendas WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultarView> lista = new List<ProdutoConsultarView>();
            while (reader.Read())
            {
                ProdutoConsultarView dto = new ProdutoConsultarView();
                dto.ID = reader.GetInt32("id_venda");
                dto.Pagamento = reader.GetString("ds_frpagamento");
                dto.QtdItens = reader.GetInt32("qtd_itens");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.Data = reader.GetDateTime("dt_data");
                dto.Total = reader.GetDecimal("vl_total");
                dto.Funcionario = reader.GetString("nm_funcionario");
                

                lista.Add(dto);
                
            }

            reader.Close();

            return lista;
        }
    

    }
}
