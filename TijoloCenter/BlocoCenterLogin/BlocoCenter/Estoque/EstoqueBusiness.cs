﻿using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Estoque
{
    class EstoqueBusiness
    {
        public int Salvar (EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Salvar(dto);
        }

        public List<EstoqueView> Consultar(string produto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Consultar(produto);
        }

        public int DarBaixa(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.DarBaixaEstoque(dto);
        }

        public List<EstoqueView> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }

        public int Adicionar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.AdicionarEstoque(dto);
        }

    }
}
