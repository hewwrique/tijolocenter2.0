﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
    class EstoqueDTO
    {


        public int id_estoque { get; set; }
        public int quantidade { get; set; }
        public int id_produto { get; set; }
        
    }
}
