﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar (EstoqueDTO dto)
        {
            string script = @" INSERT INTO tb_estoque (id_produtofornecedor, ds_quantidade) VALUES (@id_produtofornecedor, @ds_quantidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.id_produto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int DarBaixaEstoque(EstoqueDTO dto)
        {
            string script = @" UPDATE tb_estoque 
                                  SET ds_quantidade = ds_quantidade - @ds_quantidade
                                WHERE id_produtofornecedor = @id_produtofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.id_produto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<EstoqueView> Listar()
        {
            string script = "select * from VW_ESTOQUE2";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueView> listar = new List<EstoqueView>();

            while (reader.Read())
            {
                EstoqueView dto = new EstoqueView();
                dto.id_estoque = reader.GetInt32("id_estoque");
                dto.Quantidade = reader.GetInt32("ds_quantidade");
                dto.Produto = reader.GetString("nm_produtof");

                listar.Add(dto);

            }

            reader.Close();
            return listar;
        }


        public List<EstoqueView> Consultar(string produto)
        {
            string script = @"SELECT * FROM VW_ESTOQUE2 WHERE nm_produtof like @nm_produtof";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produtof", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueView> lista = new List<EstoqueView>();
            while (reader.Read())
            {
                EstoqueView dto = new EstoqueView();
                dto.id_estoque = reader.GetInt32("id_estoque");
                dto.Quantidade = reader.GetInt32("ds_quantidade");
                dto.Produto = reader.GetString("nm_produtof");
                
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public int AdicionarEstoque(EstoqueDTO dto)
        {
            string script = @" UPDATE tb_estoque 
                                  SET ds_quantidade = ds_quantidade + @ds_quantidade
                                WHERE id_produtofornecedor = @id_produtofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.id_produto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
