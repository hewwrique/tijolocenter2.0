﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Estoque
{
    class EstoqueView
    {
        public int id_estoque { get; set; }
        public int Quantidade { get; set; }
        public string Produto { get; set; }
    }
}
