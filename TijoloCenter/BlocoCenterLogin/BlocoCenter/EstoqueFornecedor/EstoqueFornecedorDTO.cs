﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
    class EstoqueFornecedorDTO
    {

        public int id_estoquefornecedor { get; set; }
        public int quantidade { get; set; }
        public int id_pedidofornecedor { get; set; }
        public int id_fornecedor { get; set; }
    }
}
